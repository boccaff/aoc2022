use std::cmp::Ordering;

const RBRACKET: u8 = ']' as u8;
const LBRACKET: u8 = '[' as u8;
const COMMA: u8 = ',' as u8;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Packet {
    Num(u8),
    List(Vec<Packet>),
}

impl Ord for Packet {
    fn cmp(&self, other: &Self)  -> std::cmp::Ordering {
        match (self, other) {
            (Packet::List(x), Packet::List(y)) => { x.cmp(y) },
            (Packet::List(x), Packet::Num(y)) => { x.cmp(&vec![Self::Num(*y)]) },
            (Packet::Num(x), Packet::List(y)) => {vec![Self::Num(*x)].cmp(y)},
            (Packet::Num(x), Packet::Num(y)) => {x.cmp(y)},
        }
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}


fn parse_number(input: &[u8]) -> (Packet, &[u8]){

    let mut end = 0_usize;
    while end < input.len() {
        if input[end] > ('9' as u8) || input[end] < ('0' as u8) {
            break
        } else {
            end += 1;
        }
    }

    let x = input[..end].iter().rev().enumerate()
            .map(|(i, x)| (*x as u8 - b'0' as u8) * 10_u8.pow(i as u32))
            .sum();
    (Packet::Num(x), &input[end..])
}

// https://nickymeuleman.netlify.app/garden/aoc2022-day13
fn parse_packet(input: &[u8]) -> (Packet, &[u8]){
    let mut input = &input[1..];
    let mut res: Vec<Packet> = Vec::new();

    loop {
        match input[0] {
            RBRACKET => break,
            COMMA => input = &input[1..],
            LBRACKET => {
                let (packet, rem) = parse_packet(input);
                res.push(packet);
                input = rem;
            },
            _ => {
                let (num, rem) = parse_number(input);
                res.push(num);
                input = rem;
            },
        }
    }
    (Packet::List(res), &input[1..])
}

pub fn parse_input(filename: &str) -> Vec<(Packet, Packet)> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read :{}", filename))
        .split("\n\n")
        .map(|packets| {
            //println!("{:?}", packets);
            let mut packets = packets.split("\n");
            let lpacket = packets.next().unwrap();
            let (lpacket,_) = parse_packet(&lpacket.bytes().collect::<Vec<u8>>());
            let rpacket = packets.next().unwrap();
            let (rpacket,_) = parse_packet(&rpacket.bytes().collect::<Vec<u8>>());
            (lpacket, rpacket)
        })
        .collect()
}

pub fn part1(input: &[(Packet, Packet)]) -> usize {
    input.iter().enumerate()
        .filter(|(i, (left, right))| left < right)
        .map(|(i, _)| i + 1)
        .sum()
}

pub fn part2(input: &[(Packet, Packet)]) -> usize {
    let mut input = input.iter()
        .map(|x| vec![x.0.clone(), x.1.clone()])
        .flatten()
        .collect::<Vec<Packet>>();
    input.push(Packet::Num(2));
    input.push(Packet::Num(6));

    input.sort();
    let p2index: usize = input.iter().enumerate().filter(|(i, p)| *p == &Packet::Num(2))
        .map(|(i, _)| i).min().unwrap() + 1;
    let p6index: usize = input.iter().enumerate().filter(|(i, p)| *p == &Packet::Num(6))
        .map(|(i, _)| i).min().unwrap() + 1;

    p2index * p6index
}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let res = part2(&input);
        assert_eq!(res, 140)
    }

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let res = part1(&input);
        assert_eq!(res, 13)
    }

    #[test]
    fn test_parse_packet(){
        let examples = vec![
            ("[1,2]", Packet::List(vec![Packet::Num(1), Packet::Num(2)])),
            ("[1,[2,3]]", Packet::List(vec![Packet::Num(1), Packet::List(vec![Packet::Num(2),Packet::Num(3)])])),
        ];
        for (input, ans) in examples.iter() {
            let input = input.bytes().collect::<Vec<u8>>();
            let (res, _)= parse_packet(&input);
            assert_eq!(res, *ans)
        }
    }

    #[test]
    fn test_parse_number(){
        let examples = vec![
            ("13", Packet::Num(13)),
            ("1,10", Packet::Num(1)),
            ("12]", Packet::Num(12)),
        ];
        for (input, ans) in examples.iter() {
            let input = input.bytes().collect::<Vec<u8>>();
            let (res, _)= parse_number(&input);
            assert_eq!(res, *ans)
        }
    }

}
