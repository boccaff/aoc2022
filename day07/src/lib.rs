type Path = Vec<String>;

#[derive(Debug, PartialEq, Clone)]
pub enum Entry {
    File {
        name: String,
        size: usize,
    },
    Dir {
        name: String,
    }
}

pub fn parse_input(filename: &str) -> Vec<(Path, Entry)>{
    let mut flat_fs = Vec::new();
    let mut current_dir: Path = Vec::new();
    flat_fs.push((vec![], Entry::Dir {name: "/".to_string()}));

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .for_each(|line| {
            let args: Vec<&str> = line.split_whitespace().collect();
            match args.as_slice() {
                ["$", "cd", ".."] => {current_dir.pop();},
                ["$", "cd", directory] => current_dir.push(directory.to_string()),
                ["$", "ls"] => (),
                ["dir", name] => {
                    flat_fs.push((current_dir.clone(), Entry::Dir { name: name.to_string() }))
                },
                [size, name] => {
                    let size: usize = size.parse().unwrap_or_else(|_| panic!("Failed to parse: {}", line));
                    flat_fs.push((current_dir.clone(), Entry::File { name: name.to_string() , size }))
                },
                _ => panic!("Failed to parse: {}", line),
            }
        });
    flat_fs
}

fn contained_in_dir(path: &Path, filesystem: &[(Path, Entry)]) -> Vec<(Path, Entry)>{
    filesystem.iter().filter(|(p, _)| p == path).map(|(p, e)|(p.to_vec(), e.clone())).collect()
}

fn total_size(contents: &[(Path, Entry)], filesystem: &[(Path, Entry)]) -> usize {
    contents
        .iter()
        .fold(0, |acc, (p, e)| match e {
            Entry::Dir {name} => {
                let mut subdir = p.clone();
                subdir.push(name.to_string());
                let subdir_contents = contained_in_dir(&subdir, filesystem);
                total_size(&subdir_contents, filesystem) + acc
            },
            Entry::File {name: _ , size} => { size + acc },
        })
}

fn folder_size(filesystem: &[(Path, Entry)]) -> Vec<(Path, usize)> {
    let fs = filesystem.iter()
        .filter(|(_, entry)|
                match entry {
                    Entry::Dir { name: _ } => true,
                    _ => false,
                })
    .map(|(path, entry)| {
        let mut path = path.clone();
        let name = if let Entry::Dir { name } = entry { name } else {panic!()};
        path.push(name.to_string());
        let contents = contained_in_dir(&path, filesystem);
        let s = total_size(&contents, filesystem);
        (path.to_vec(), s)
    })
    .collect();
    fs
}

pub fn part1(filesystem: &[(Path, Entry)]) -> usize {
    folder_size(filesystem)
        .iter()
        .filter(|(_, s)| *s < 100_000)
        .map(|(_, s)| *s)
        .sum()
}

pub fn part2(filesystem: &[(Path, Entry)]) -> usize {
    let fsizes = folder_size(filesystem);
    let root_size: usize = fsizes.iter().filter(|(p, _)| *p == vec!["/"]).map(|(_, e)| *e).sum();
    let free_size = 70_000_000 - root_size;
    let required = 30_000_000 - free_size;

    fsizes
        .iter()
        .filter(|(_, s)| *s > required)
        .map(|(_, s)| *s)
        .min()
        .unwrap()
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 95437)
    }

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 24933642)
    }

}
