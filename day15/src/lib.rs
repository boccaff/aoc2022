use std::cmp::Ordering;
use std::collections::HashSet;

#[derive(PartialEq, Eq, Debug, Clone, Copy, Hash)]
pub struct Point2D {
    x: isize,
    y: isize,
}

impl Ord for Point2D {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.x, self.y).cmp(&(other.x, other.y))
    }
}

impl PartialOrd for Point2D {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn get_number_in_string(input: &str) -> isize {
    let sign: isize = if input.contains("-") { -1 } else { 1 };

    let x: isize = input.bytes()
        .rev()
        .enumerate()
        .map(|(i, x)|{
            if x >= ('0' as u8) && x <= ('9' as u8){
                (x - &('0' as u8)) as isize * 10_isize.pow(i as u32) as isize
            } else {
                0
            }
        })
        .sum();
    sign * x
}

fn parse_coords(coords: &str) -> Point2D {
    let (xstr, ystr) = coords.split_once(", ").unwrap();
    Point2D {x: get_number_in_string(xstr), y: get_number_in_string(ystr)}
}

type Input = (Vec<Point2D>, Vec<Point2D>);

pub fn parse_input(filename: &str) -> Input {

    let mut beacons: Vec<Point2D> = Vec::new();
    let mut sensors: Vec<Point2D> = Vec::new();

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("could not read: {}", filename))
        .lines()
        .for_each(|line| {
            let (sensor, beacon) = line.split_once(": closest beacon is at ").unwrap();
            sensors.push(parse_coords(sensor));
            beacons.push(parse_coords(beacon));
        });
    (beacons, sensors)
}

fn manhattan(p1: &Point2D, p2: &Point2D) -> isize {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

#[allow(dead_code)]
fn find_nearest_point(point: &Point2D, candidates: &[Point2D]) -> (isize, Point2D) {

    let mut ans = candidates.iter()
        .map(|candidate| (manhattan(candidate, point), *candidate))
        .collect::<Vec<(isize, Point2D)>>();
    ans.sort();
    *ans.first().unwrap()

}

fn generate_neighbors(point: &Point2D, distance: isize) -> Vec<Point2D> {
    (-distance..=distance).map(move |dx|{
        (-(distance - dx.abs())..=(distance - dx.abs()))
            .map(|dy|{
                Point2D { x: point.x + dx, y: point.y + dy }
            }).collect::<Vec<Point2D>>()
    })
    .flatten()
    .collect()
}

pub fn something(){
    //let input = parse_input("./example.txt");
    let p = Point2D{x: 0, y: 0};
    for p in generate_boundary(&p, 2).iter(){
        println!("{:?}", p);
    }
}

fn generate_boundary(point: &Point2D, distance: isize) -> Vec<Point2D> {

    [(1, 1), (-1, 1), (1, -1), (-1, -1)]
        .iter().map(|(sx, sy)|{
        (0..=distance).map(|x|{
            Point2D {x: point.x + sx*x, y: point.y + sy*(distance - x)}
        }).collect::<Vec<Point2D>>()
        }).flatten()
    .collect()
}

fn print_result(beacons: &[Point2D], sensors: &[Point2D], set: &HashSet<Point2D>){

    let xmin = set.iter().map(|p| p.x).min().unwrap();
    let ymin = set.iter().map(|p| p.y).min().unwrap();
    let xmax = set.iter().map(|p| p.x).max().unwrap();
    let ymax = set.iter().map(|p| p.y).max().unwrap();

    // the offset for proper print
    //      12345
    print!("     ");
    for xi in xmin..=xmax{
        if xi % 5 == 0  && xi < 10{
            print!("{}", xi);
        } else {
            print!(" ");
        }
    }
    println!("");
    for yi in ymin..=ymax {
        print!("{:03}: ", yi);
        for xi in xmin..=xmax {
            if beacons.iter().any(|p| p.x == xi && p.y == yi){
                print!("B");
            } else if sensors.iter().any(|p| p.x == xi && p.y == yi){
                print!("S");
            } else if set.contains(&Point2D{x: xi, y: yi}) {
                print!("#");
            } else {
                print!(".");
            }
        }
        print!("\n");
    }
}

pub fn brute_force(input: &Input) -> HashSet<Point2D> {

    let (beacons, sensors) = input.clone();

    let mut set: HashSet<Point2D> = HashSet::new();

    for (beacon, sensor) in beacons.iter().zip(sensors.iter()) {
        let dist = manhattan(beacon, sensor);
        generate_neighbors(sensor, dist)
            .iter()
            .for_each(|p| {set.insert(*p);});
    }

    set
}

fn merge_ranges(ranges: &[(isize, isize)]) -> Vec<(isize, isize)>{
	let mut merged = Vec::new();
	let mut ranges: Vec<_> = ranges.into_iter().collect();
	ranges.sort_by(|a,b| a.0.cmp(&b.0));

	let mut working_tuple = *ranges.pop().unwrap();

	while let Some(range) = ranges.pop() {
		if (working_tuple.0 >= range.0 && working_tuple.0 <= range.1) ||
			(range.1 >= working_tuple.0 && range.1 <= working_tuple.1) {
			working_tuple = (working_tuple.0.min(range.0), working_tuple.1.max(range.1));
		} else {
			merged.push(working_tuple);
			working_tuple = *range;
		}
	}
	merged.push(working_tuple);
    let merged: Vec<_> = merged.into_iter().rev().collect();
	merged
}

fn possible_beacons(input: &Input, line: isize) -> Vec<(isize, isize)>{
    let mut ranges: Vec<(isize, isize)> = Vec::new();

    for (beacon, sensor) in input.0.iter().zip(input.1.iter()) {
        let dist = manhattan(beacon, sensor);
        let delta = dist - (sensor.y - line).abs();
        if delta > 0 {
            let range = (sensor.x - delta, sensor.x + delta);
            ranges.push(range);
        }
    }
	merge_ranges(&ranges)
}


pub fn part1(input: &Input, line: isize) -> usize {

	let s: usize = possible_beacons(input, line).iter().map(|x| (x.0..x.1).len() + 1_usize).sum();
    let beacons: HashSet<Point2D> = input.0.iter().filter(|p| p.y == line).map(|p| *p).collect();

	s - beacons.len()
}

pub fn part2(input: &Input, limit: isize) -> isize {

    for y_coord in 0..=limit {
        let ranges = possible_beacons(input, y_coord);
        if ranges.iter().any(|range| range.0<= 0 && range.1 >= limit){
            continue
        } else{
            for x_coord in 0..=limit{
                if !ranges.iter().any(|range| x_coord >= range.0 && x_coord <= range.1) {
                    return x_coord * 4_000_000 + y_coord;
                }
            }
        }
    }
    unreachable!()
}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part2(){
        let input = parse_input("./example.txt");
        let p2 = part2(&input, 20);
        assert_eq!(p2, 56000011)
    }

    #[test]
    fn test_part1(){
        let input = parse_input("./example.txt");
        let p1 = part1(&input, 10);
        assert_eq!(p1, 26)
    }

    #[test]
    fn test_get_number(){
        let tests = vec![
            ("Sensor at x=2", 2),
            ("Sensor at x=-10", -10),
            ("x=23", 23),
            ("y=100", 100),
            ("y=-123", -123),
        ];
        for (input, ans) in tests.iter(){
            assert_eq!(get_number_in_string(input), *ans)
        }
    }
}

