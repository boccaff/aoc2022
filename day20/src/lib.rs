pub fn parse_input(filename: &str) -> Vec<isize> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|x| x.parse().unwrap())
        .collect::<Vec<_>>()
}

fn mix(input: &[isize], key: isize, reps: isize) -> Vec<isize> {
    let nums: Vec<isize> = input.iter().map(|x| *x * key).collect();
    let mut ans: Vec<(usize, isize)> = nums.iter().map(|&x| x).enumerate().collect();

    for _ in 0..reps {
        for (pos, num) in nums.iter().enumerate() {
            let ans_pos = ans.iter().position(|(i, _)| *i == pos).unwrap();
            let tmp = ans.remove(ans_pos);
            let new_pos = (ans_pos as isize + *num).rem_euclid(ans.len() as isize) as usize;
            //ans.insert(if new_pos == 0 { ans.len() } else { new_pos }, tmp);
            ans.insert(new_pos, tmp);
        }
    }
    ans.iter().map(|(_, x)| *x).collect()
}

pub fn part1(input: &[isize]) -> isize {
    let ans = mix(input, 1, 1);
    let zero_pos = ans.iter().position(|x| *x == 0).unwrap();
    [1000, 2000, 3000]
        .iter()
        .map(|x| ans[(zero_pos + x).rem_euclid(ans.len())])
        .sum()
}

pub fn part2(input: &[isize]) -> isize {
    let ans = mix(input, 811589153, 10);
    let zero_pos = ans.iter().position(|x| *x == 0).unwrap();
    [1000, 2000, 3000]
        .iter()
        .map(|x| ans[(zero_pos + x).rem_euclid(ans.len())])
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 3)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 1623178306)
        // 2653084941157 too high
    }
}
