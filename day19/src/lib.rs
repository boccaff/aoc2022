#[macro_use]
extern crate scan_fmt;

use hashbrown::HashSet;
use rayon::prelude::*;

type SevenIntTuple = (i16, i16, i16, i16, i16, i16, i16);

#[derive(Debug, Eq, PartialEq)]
pub struct Blueprint {
    id: i16,
    ore_bot_ore_cost: i16,
    clay_bot_ore_cost: i16,
    obs_bot_ore_cost: i16,
    obs_bot_clay_cost: i16,
    geo_bot_ore_cost: i16,
    geo_bot_obs_cost: i16,
}
#[derive(Debug, Eq, PartialEq, Hash, Clone)]
struct State {
    ore_bot: i16,
    clay_bot: i16,
    obs_bot: i16,
    geo_bot: i16,
    ore: i16,
    clay: i16,
    obs: i16,
    geo: i16,
}

impl Blueprint {
    fn from_tuple(t: &SevenIntTuple) -> Self {
        Blueprint {
            id: t.0,
            ore_bot_ore_cost: t.1,
            clay_bot_ore_cost: t.2,
            obs_bot_ore_cost: t.3,
            obs_bot_clay_cost: t.4,
            geo_bot_ore_cost: t.5,
            geo_bot_obs_cost: t.6,
        }
    }
}

impl State {
    fn from_tuple(t: &(i16, i16, i16, i16, i16, i16, i16, i16)) -> Self {
        State {
            ore_bot: t.0,
            clay_bot: t.1,
            obs_bot: t.2,
            geo_bot: t.3,
            ore: t.4,
            clay: t.5,
            obs: t.6,
            geo: t.7,
        }
    }

    fn step(&self) -> Self {
        State {
            ore: self.ore + self.ore_bot,
            clay: self.clay + self.clay_bot,
            obs: self.obs + self.obs_bot,
            geo: self.geo + self.geo_bot,
            ..self.clone()
        }
    }

    fn explore(&self, bp: &Blueprint) -> Vec<Self> {
        let mut states = Vec::new();
        let next_state = self.step();
        let max_ore_cost = bp.geo_bot_ore_cost.max(
            bp.ore_bot_ore_cost
                .max(bp.clay_bot_ore_cost.max(bp.obs_bot_ore_cost)),
        );
        let max_clay_cost = bp.clay_bot_ore_cost.max(bp.obs_bot_clay_cost);

        // build geo bot if possible
        if bp.geo_bot_ore_cost <= self.ore && bp.geo_bot_obs_cost <= self.obs {
            states.push(State {
                geo_bot: next_state.geo_bot + 1,
                ore: next_state.ore - bp.geo_bot_ore_cost,
                obs: next_state.obs - bp.geo_bot_obs_cost,
                ..next_state
            });
            return states;
        }

        // build ore bot if possible
        if bp.ore_bot_ore_cost <= self.ore && next_state.ore_bot < max_ore_cost {
            states.push(State {
                ore_bot: next_state.ore_bot + 1,
                ore: next_state.ore - bp.ore_bot_ore_cost,
                ..next_state
            })
        }

        // build clay bot if possible
        if bp.clay_bot_ore_cost <= self.ore && next_state.clay_bot < max_clay_cost {
            states.push(State {
                clay_bot: next_state.clay_bot + 1,
                ore: next_state.ore - bp.clay_bot_ore_cost,
                ..next_state
            })
        }

        // build obs bot if possible
        if bp.obs_bot_ore_cost <= self.ore && bp.obs_bot_clay_cost <= self.clay {
            states.push(State {
                obs_bot: next_state.obs_bot + 1,
                ore: next_state.ore - bp.obs_bot_ore_cost,
                clay: next_state.clay - bp.obs_bot_clay_cost,
                ..next_state
            })
        }

        // do nothing
        states.push(next_state);
        states
    }
}

fn parse_line(line: &str) -> SevenIntTuple {
    let template = "Blueprint {d}: Each ore robot costs {d} ore. Each clay robot costs {d} ore. Each obsidian robot costs {d} ore and {d} clay. Each geode robot costs {d} ore and {d} obsidian.";
    let li = scan_fmt!(line, template, i16, i16, i16, i16, i16, i16, i16).unwrap();
    li
}

pub fn parse_input(filename: &str) -> Vec<SevenIntTuple> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(parse_line)
        .collect::<Vec<_>>()
}

fn process_blueprint(blueprint: &Blueprint, t: isize) -> i16 {
    let start = State::from_tuple(&(1, 0, 0, 0, 0, 0, 0, 0));
    let mut states: HashSet<State> = HashSet::new();
    let mut best_state = 0;
    states.insert(start.clone());

    for i in 0..t {
        let mut new_states: HashSet<State> = HashSet::new();
        for state in states.iter() {
            state.explore(blueprint).iter().for_each(|x| {
                //let n = 23 - i;
                //let new_potential = x.geo + x.geo_bot * n;
                let new_potential = 0;
                if new_potential >= best_state {
                    new_states.insert(x.clone());
                    best_state = new_potential;
                }
            });
        }
        states = new_states;
    }
    states.iter().map(|x| x.geo).max().unwrap()
}

pub fn part1(input: &[SevenIntTuple]) -> i16 {
    let res: Vec<i16> = input
        .par_iter()
        .map(|x| Blueprint::from_tuple(&x))
        .map(|x| x.id * process_blueprint(&x, 24))
        .collect();

    res.iter().sum()
}

pub fn part2(input: &[SevenIntTuple]) -> i16 {
    let res: Vec<i16> = input
        .par_iter()
        .take(3)
        .map(|x| Blueprint::from_tuple(&x))
        .map(|x| process_blueprint(&x, 32))
        .collect();

    res.iter().fold(1, |acc, x| acc * x)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 33)
    }
    // 1589

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 3472)
    }
}
