#[derive(Debug, Clone)]
pub struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    test: (usize, usize, usize),
}

#[derive(Debug, Clone)]
enum Operation {
    Sum(usize),
    Mul(usize),
    Pow,
}

fn parse_monkey(input: &str) -> Monkey {
    let mut input = input.lines();
    input.next(); // discard first identification

    let (_, items) = input.next().unwrap().split_once(": ").unwrap();
    let items: Vec<usize> = items.split(", ")
        .map(|x| x.parse::<usize>().unwrap_or_else(|_| panic!("At: {:?}", items)))
        .collect();

    let  (_, last) = input.next().unwrap().split_once(" = ").unwrap();
    let mut operation: Operation;

    match &last[4..]{
        "* old" => {operation = Operation::Pow;},
        x => {
            match &x[..1_usize] {
                "+" => {
                    let x = x[2..].parse::<usize>().unwrap();
                    operation = Operation::Sum(x);
                },
                "*" => {
                    let x = x[2..].parse::<usize>().unwrap();
                    operation = Operation::Mul(x);
                },
                _ => {panic!("Failed to parse operation: {}", last)}
            }
        }
    }
    let div_check = input.next().unwrap()
        .split_once("by ").unwrap().1
        .parse::<usize>().unwrap();

    let dest_true = input.next().unwrap()
        .split_once("monkey ").unwrap().1
        .parse::<usize>().unwrap();

    let dest_false = input.next().unwrap()
        .split_once("monkey ").unwrap().1
        .parse::<usize>().unwrap();

	Monkey {items, operation, test: (div_check, dest_true, dest_false)}

}

pub fn parse_input(filename: &str) -> Vec<Monkey> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .split("\n\n")
        .map(|x| parse_monkey(x))
        .collect()
}

pub fn part1(input: &[Monkey]) -> usize {
    let mut monkeys: Vec<Monkey> = input.clone().to_vec();
    let mut tracker = vec![0; input.len()];
    let mut item: usize;
    let mut worry: usize;

    for n in 0..20 {
        for i in 0..monkeys.len() {
            while let Some(mut item) = monkeys[i].items.pop() {
                match monkeys[i].operation {
                    Operation::Pow => { item = item * item },
                    Operation::Mul(x) => {item = item * x },
                    Operation::Sum(x) => {item = item + x },
                }

                let (div_check, ift, iff) = monkeys[i].test;
                worry = item / 3;
                if worry % div_check == 0 {
                    monkeys[ift].items.push(worry);
                    tracker[i] += 1;
                } else {
                    monkeys[iff].items.push(worry);
                    tracker[i] += 1;
                };
            }
        }
    }
    tracker.sort();
    tracker.iter().rev().take(2).product()
}

pub fn part2(input: &[Monkey]) -> usize {
    let mut monkeys: Vec<Monkey> = input.clone().to_vec();
    let mut tracker = vec![0; input.len()];
    let mut item: usize;
    let mut worry: usize;
    let lcm = input.iter().map(|x| x.test.0).product::<usize>() as usize;

    for n in 0..10_000 {
        for i in 0..monkeys.len() {
            while let Some(mut item) = monkeys[i].items.pop() {
                match monkeys[i].operation {
                    Operation::Pow => { item = item * item },
                    Operation::Mul(x) => {item = item * x },
                    Operation::Sum(x) => {item = item + x },
                }

                let (div_check, ift, iff) = monkeys[i].test;
                if item % div_check == 0 {
                    monkeys[ift].items.push(item % lcm);
                    tracker[i] += 1;
                } else {
                    monkeys[iff].items.push(item % lcm);
                    tracker[i] += 1;
                };
            }
        }
    }
    tracker.sort();
    tracker.iter().rev().take(2).product()
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 10605)
    }

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 2713310158)
    }
}
