use std::collections::HashMap;

fn parse_digits(c: char) -> isize {
    match c {
        '=' => -2,
        '-' => -1,
        '0' => 0,
        '1' => 1,
        '2' => 2,
        _ => panic!("Invalid char: '{}' passed to parse digits.", c),
    }
}

fn unparse_digit(d: isize) -> char {
    match d {
        -2 => '=',
        -1 => '-',
        0 => '0',
        1 => '1',
        2 => '2',
        _ => panic!("Invalid digit: '{}' passed to parse digits.", d),
    }
}

pub fn parse_input(filename: &str) -> String {
    std::fs::read_to_string(filename).unwrap_or_else(|_| panic!("Failed to read: {:?}", filename))
}

fn parse_snafu(s: &str) -> isize {
    s.chars()
        .rev()
        .enumerate()
        .map(|(i, c)| parse_digits(c) * 5_isize.pow(i as u32))
        .sum()
}

fn parse_to_snafu(x: isize) -> String {
    let mut res = Vec::new();
    let mut x = x;
    //println!("{:?}", (x));

    let dtc = HashMap::from([
        (0_isize, '0'),
        (1_isize, '1'),
        (2_isize, '2'),
        (3_isize, '='),
        (4_isize, '-'),
    ]);

    while x > 0 {
        let rem = x.rem_euclid(5);
        res.push(dtc[&rem]);
        x = (x + 2) / 5;
        //println!("{:?}", (x, &res));
    }
    res.iter().rev().map(|x| *x).collect::<String>()
}

pub fn part1(input: &String) -> String {
    let n = input.lines().map(|x| parse_snafu(x)).sum();
    println!("{:?}", n);
    parse_to_snafu(n)
}

#[cfg(test)]
mod test {
    use super::*;

    fn test_parser() {
        [
            (1, "1"),
            (2, "2"),
            (3, "1="),
            (4, "1-"),
            (5, "10"),
            (6, "11"),
            (7, "12"),
            (8, "2="),
            (9, "2-"),
            (10, "20"),
            (15, "1=0"),
            (20, "1-0"),
            (2022, "1=11-2"),
            (12345, "1-0---0"),
            (314159265, "1121-1110-1=0"),
        ]
        .iter()
        .for_each(|(res, input)| assert_eq!(*res, parse_snafu(input)))
    }

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, "2=-1=0")
    }

    //#[test]
    //fn test_part2() {
    //let input = parse_input("example.txt");
    //let p2 = part2(&input);
    //assert_eq!(p2, 301)
    //}
}
