use day25::{parse_input, part1};

fn main() {
    let input = parse_input("input.txt");
    let p1 = part1(&input);
    println!("Part 1: {}", p1);
}
