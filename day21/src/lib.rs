use std::collections::HashMap;

type Inputs = (
    HashMap<String, usize>,
    HashMap<usize, isize>,
    HashMap<usize, (usize, char, usize)>,
);

pub fn parse_input(filename: &str) -> Inputs {
    let mut key: HashMap<String, usize> = HashMap::new();
    let mut nums: HashMap<usize, isize> = HashMap::new();
    let mut ops: HashMap<usize, (usize, char, usize)> = HashMap::new();

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .enumerate()
        .for_each(|(i, line)| {
            let (name, _) = line.split_once(": ").unwrap();
            key.insert(name.to_string(), i);
        });

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .for_each(|line| {
            let (name, remainder) = line.split_once(": ").unwrap();
            if ['/', '*', '+', '-']
                .iter()
                .any(|op| remainder.contains(*op))
            {
                let tmp: Vec<String> = remainder.split(" ").map(|x| x.to_string()).collect();
                let m1 = &tmp[0_usize];
                let m2 = &tmp[2_usize];
                let c = tmp[1_usize].chars().collect::<Vec<char>>();
                ops.insert(key[name], (key[m1], *c.first().unwrap(), key[m2]));
            } else {
                nums.insert(key[name], remainder.parse().unwrap());
            }
        });
    (key, nums, ops)
}

pub fn part1(input: &Inputs) -> isize {
    let (m1, m2) = inner_part2(input, input.1[&input.0["humn"]]);
    m1 + m2
}

fn inner_part2(input: &Inputs, guess: isize) -> (isize, isize) {
    let (key, mut nums, mut ops) = input.clone();
    nums.insert(key["humn"], guess);
    while !nums.contains_key(&key["root"]) {
        let ops_keys = ops.keys().map(|x| *x).collect::<Vec<usize>>();
        for k in ops_keys.iter() {
            let (m1, op, m2) = &ops[k].clone();
            if nums.contains_key(m1) && nums.contains_key(m2) {
                if k == &key["root"] {
                    return (nums[m1], nums[m2]);
                }
                ops.remove(k);
                match op {
                    '/' => {
                        nums.insert(*k, nums[m1] / nums[m2]);
                    }
                    '*' => {
                        nums.insert(*k, nums[m1] * nums[m2]);
                    }
                    '+' => {
                        nums.insert(*k, nums[m1] + nums[m2]);
                    }
                    '-' => {
                        nums.insert(*k, nums[m1] - nums[m2]);
                    }
                    _ => unreachable!(),
                }
            }
        }
    }
    unreachable!()
}

pub fn part2(input: &Inputs) -> isize {
    let mut left = -10_isize.pow(18);
    let mut right = 10_isize.pow(18);

    let m = inner_part2(input, (left + right) / 2);
    let up = m.1 - m.0 > 0;

    while left + 1 < right {
        let mid = (left + right) / 2;
        let check = inner_part2(input, mid);

        if check.0 == check.1 {
            for x in [-5, -4, -3, -2, -1, 0] {
                let check = inner_part2(input, mid + x);
                if check.0 == check.1 {
                    return mid + x;
                }
            }
        } else if (check.0 < check.1 && up) || (check.0 > check.1 && !up) {
            left = mid;
        } else {
            right = mid;
        }
    }
    unreachable!()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 152)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 301)
    }
}
