use std::collections::HashMap;
use std::collections::BinaryHeap;

#[derive(Debug, Clone)]
pub struct Map {
    height: HashMap<(usize, usize), u32>,
    start: (usize, usize),
    end: (usize, usize),
}

pub fn parse_input(filename: &str) -> Map {

    let mut start: (usize, usize) = (0, 0);
    let mut end: (usize, usize) = (0, 0);
    let mut height: HashMap<(usize, usize), u32> = HashMap::new();


    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .enumerate()
        .for_each(|(i, line)| {
            line.chars().enumerate().for_each(|(j, c)|{
                match c {
                    'S' => { start = (i, j); },
                    'E' => { end = (i, j); },
                    _ => {
                        let h = c as u32 - 'a' as u32;
                        height.insert((i, j), h);
                    }
                }
            })
        });
    height.insert(start, 0);
    height.insert(end, 'z' as u32 - 'a' as u32);

    Map { height, start, end }
}

fn find_neighbors(map: &Map, from: (usize, usize)) -> Vec<(usize, usize)> {
    [(0 , 1), (0 , -1), (1 , 0), (-1, 0)]
        .iter()
        .filter_map(|(dx, dy)| {
            let candidate = (from.0 as isize + dx, from.1 as isize + dy);
            if candidate.0 >= 0 || candidate.1 >= 0 {
                let candidate = (candidate.0 as usize, candidate.1 as usize);
                if map.height.contains_key(&from) && map.height.contains_key(&candidate) {
                    let hf = map.height.get(&from).unwrap();
                    let hc = map.height.get(&candidate).unwrap();
                    if hc <= &(hf + 1){
                        Some(candidate)
                    } else {
                        None
                    }
                } else {
                    None
                }
            } else {
                None
            }
        })
    .collect()
}

fn shortest_path(map: &Map) -> Option<isize>{
    let mut dist: HashMap<(usize, usize), isize> = HashMap::new();
    let mut heap: BinaryHeap<(isize, (usize, usize))> = BinaryHeap::new();

    dist.insert(map.start, 0);
    heap.push((0, (map.start)));

    while let Some((cost, position)) = heap.pop() {
        let cost = - cost;
        if position == map.end { return Some(cost) }

        if cost > *dist.entry(position).or_insert(isize::MAX) {continue;}

        for neighbor in find_neighbors(map, position).iter(){

            let mut next = ( cost + 1,  *neighbor);

            if next.0 < *dist.entry(*neighbor).or_insert(isize::MAX) {
                dist.insert(*neighbor, next.0);
                next.0 = - next.0;
                heap.push(next);
            }
        }
    }

    None
}

pub fn part1(map: &Map) -> isize {
    if let Some(ans )= shortest_path(map) {
        ans
    } else {
        panic!("Failed to find a path!")
    }
}

pub fn part2(map: &Map) -> isize {

    map.height
        .iter()
        .filter(|(k, v)| **v == 0 && **k != (0, 0))
        .filter_map(|(k, _)|{
            let mut map2 = map.clone();
            map2.start = *k;
            shortest_path(&map2)
        })
        .min()
        .unwrap_or_else(||panic!("Failed to find a path!"))

}



#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 31)
    }

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 29)
    }
}
