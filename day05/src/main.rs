use day05::{parse_input, part1, part2};

fn main() {
    let (stacks, orders) = parse_input("input.txt");
    let p1 = part1(stacks.clone(), &orders);
    println!("{:?}", p1);

    let p2 = part2(stacks, &orders);
    println!("{:?}", p2);
}
