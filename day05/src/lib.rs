pub fn parse_input(filename: &str) -> (Vec<Vec<char>>, Vec<(usize, usize, usize)>) {
    if let Some((raw_stacks, raw_orders)) = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .split_once("\n\n")
    {
        let n_stacks = &raw_stacks
            .lines()
            .rev()
            .take(1)
            .collect::<String>()
            .split_whitespace()
            .rev()
            .take(1)
            .collect::<String>()
            .parse::<usize>()
            .unwrap();

        let mut stacks: Vec<Vec<char>> = Vec::new();

        (0..*n_stacks).for_each(|_| stacks.push(Vec::new()));

        raw_stacks.lines().rev().skip(1).for_each(|x| {
            for (i, c) in x.chars().skip(1).step_by(4).enumerate() {
                if c.is_alphabetic() {
                    stacks[i].push(c)
                }
            }
        });
        let orders = raw_orders
            .lines()
            .map(|x| {
                let (n, directions) = x[5..].split_once(" from ").unwrap();
                let n: usize = n.parse().unwrap();
                let (from, to) = directions.split_once(" to ").unwrap();
                let from: usize = from.parse().unwrap();
                let to: usize = to.parse().unwrap();
                (n, from - 1, to - 1)
            })
            .collect::<Vec<(usize, usize, usize)>>();

        return (stacks, orders);
    } else {
        panic!()
    };
}

pub fn part1(start_stacks: Vec<Vec<char>>, orders: &[(usize, usize, usize)]) -> String {
    let mut stacks = start_stacks.clone();
    for (n, from, to) in orders.iter() {
        for _ in 0..*n {
            if let Some(m) = stacks[*from].pop() {
                stacks[*to].push(m);
            }
        }
    }
    stacks.iter().filter_map(|x| x.last()).collect::<String>()
}

pub fn part2(start_stacks: Vec<Vec<char>>, orders: &[(usize, usize, usize)]) -> String {
    let mut stacks = start_stacks.clone();
    for (n, from, to) in orders.iter() {
        let pos = stacks[*from].len() - n;
        let crates: Vec<_> = stacks[*from].drain(pos..).collect();
        stacks[*to].extend(crates);
    }
    stacks.iter().filter_map(|x| x.last()).collect::<String>()
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let (stacks, orders) = parse_input("example.txt");
        let p1 = part1(stacks, &orders);
        assert_eq!(p1, "CMZ".to_string())
    }

    #[test]
    fn test_part2() {
        let (stacks, orders) = parse_input("example.txt");
        let p2 = part2(stacks, &orders);
        assert_eq!(p2, "MCD".to_string())
    }
}
