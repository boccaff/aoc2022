
pub fn parse_input(filename: &str) -> String {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
}

pub fn find_distinct_set(input: &str, setsize: usize) -> usize {
    'outer: for (i, x) in input.as_bytes().windows(setsize).enumerate(){
        for i in 0..setsize {
            for j in i..setsize {
                if i != j {
                    if x[i] == x[j] {
                        continue 'outer;
                    }
                }
            }
        }
        return i + setsize
    }
    panic!()
}

pub fn part1(input: &str) -> usize {
    find_distinct_set(input, 4)
}

pub fn part2(input: &str) -> usize {
    find_distinct_set(input, 14)
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let examples = vec![
            ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7),
            ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
            ("nppdvjthqldpwncqszvftbrmjlhg", 6),
            ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
            ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11),
        ];
        for (input, ans) in examples.iter(){
            let p1 = part1(input);
            assert_eq!(p1, *ans)
        }
    }

    #[test]
    fn test_part2() {
        let examples = vec![
            ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
            ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
            ("nppdvjthqldpwncqszvftbrmjlhg", 23),
            ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
            ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26),
        ];
        for (input, ans) in examples.iter(){
            let p2 = part2(input);
            assert_eq!(p2, *ans)
        }
    }

}
