fn priorities(c: &char) -> usize {
    if c.is_ascii_uppercase() {
        *c as usize - 38_usize
    } else {
        *c as usize - 96_usize
    }
}

fn part1(rucksacks: &[String]) -> usize {
    rucksacks.iter()
        .map(|rs| {
            let l = rs.len() / 2;
            let mut tmp: Vec<char> = Vec::new();
            rs[..l].chars().for_each(|x| {
                if rs[l..].contains(x) && !tmp.contains(&x){
                    tmp.push(x);
                }
            });
            tmp.iter().map(|x| priorities(x)).sum::<usize>()
        })
    .sum()
}

fn part2(mut rsacks: Vec<String>) -> usize{
    let mut s: usize = 0;
    while let (Some(x), Some(y), Some(z)) = (rsacks.pop(), rsacks.pop(), rsacks.pop()) {
        let mut tmp: Vec<char> = Vec::new();
        x.chars().for_each(
            |c|
            {
                if y.contains(c) && z.contains(c) && !tmp.contains(&c){
                    s += priorities(&c);
                    tmp.push(c)
                }
            }
            );
    }
    s
}

fn parse_input(filename: &str) -> Vec<String> {
    //let mut res = HashSet::new();

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read {}", filename))
        .lines()
        .map(|x| x.to_string())
        .collect()
}

fn main() {
    let input = parse_input("input.txt");
    //println!("{:?}", input);
    let p1 = part1(&input);
    println!("Part 1: {}", p1);
    let p2 = part2(input);
    println!("Part 2: {}", p2);
}
