pub fn parse_input(filename: &str) -> Vec<u8> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read {}", filename))
        .trim()
        .as_bytes()
        .to_vec()
}

#[derive(Debug, Eq, PartialEq)]
enum Rock {
    Horizontal,
    Plus,
    ReverseL,
    Vertical,
    Square,
}

impl Rock {
    fn get_rel_coordinates(&self) -> Vec<(usize, usize)> {
        // Occupied positions from bottom left
        match self {
            Rock::Horizontal => vec![
                (0_usize, 0_usize),
                (1_usize, 0_usize),
                (2_usize, 0_usize),
                (3_usize, 0_usize),
            ],
            Rock::Plus => vec![
                (1_usize, 0_usize),
                (0_usize, 1_usize),
                (1_usize, 1_usize),
                (2_usize, 1_usize),
                (1_usize, 2_usize),
            ],
            Rock::ReverseL => vec![
                (0_usize, 0_usize),
                (1_usize, 0_usize),
                (2_usize, 0_usize),
                (2_usize, 1_usize),
                (2_usize, 2_usize),
            ],
            Rock::Vertical => vec![
                (0_usize, 0_usize),
                (0_usize, 1_usize),
                (0_usize, 2_usize),
                (0_usize, 3_usize),
            ],
            Rock::Square => vec![
                (0_usize, 0_usize),
                (1_usize, 0_usize),
                (0_usize, 1_usize),
                (1_usize, 1_usize),
            ],
        }
    }
    fn move_rock(&self, start: &(usize, usize), direction: &u8) -> Vec<(usize, usize)> {
        let res: Vec<(usize, usize)> = self
            .get_abs_coordinates(start)
            .iter()
            .map(|(x, y)| match direction {
                62_u8 => (x + 1_usize, *y),
                60_u8 => (x - 1_usize, *y),
                0_u8 => (*x, y - 1_usize),
                _ => panic!("Invalid move direction: {}", direction),
            })
            .collect();
        res
    }

    fn get_abs_coordinates(&self, start: &(usize, usize)) -> Vec<(usize, usize)> {
        self.get_rel_coordinates()
            .iter()
            .map(|(x, y)| (start.0 + x, start.1 + y))
            .collect()
    }

    fn max_y(&self) -> usize {
        self.get_rel_coordinates()
            .iter()
            .map(|(_, y)| *y)
            .fold(0_usize, |acc, x| acc.max(x))
    }
    #[allow(dead_code)]
    fn min_y(&self) -> usize {
        self.get_rel_coordinates()
            .iter()
            .map(|(_, y)| *y)
            .fold(usize::MAX, |acc, x| acc.min(x))
    }
    fn max_x(&self) -> usize {
        self.get_rel_coordinates()
            .iter()
            .map(|(x, _)| *x)
            .fold(0_usize, |acc, x| acc.max(x))
    }
    fn min_x(&self) -> usize {
        self.get_rel_coordinates()
            .iter()
            .map(|(x, _)| *x)
            .fold(usize::MAX, |acc, x| acc.min(x))
    }

    fn can_fall(&self, start: &(usize, usize), cave: &[Vec<bool>]) -> bool {
        if start.1 == 0_usize {
            false
        } else {
            let new_positions = self.move_rock(start, &0_u8);
            return new_positions.iter().all(|(x, y)| !cave[*y][*x]);
        }
    }

    fn can_move(&self, start: &(usize, usize), direction: &u8, cave: &[Vec<bool>]) -> bool {
        let dir_move = match direction {
            62 => {
                // right
                self.max_x() + start.0 < 6
            }
            60 => {
                // left
                self.min_x() + start.0 > 0
            }
            0 => start.1 > 0,
            _ => panic!("Invalid direction move: {}", direction),
        };
        if !dir_move {
            false
        } else {
            let new_positions = self.move_rock(start, direction);
            return new_positions.iter().all(|(x, y)| !cave[*y][*x]);
        }
    }
}

#[allow(dead_code, clippy::ptr_arg)]
fn print_chamber_and_rock(chamber: &Vec<Vec<bool>>, rock: &Rock, coords: &(usize, usize)) {
    let mut c2 = chamber.clone();
    rock.get_abs_coordinates(coords)
        .iter()
        .for_each(|(x, y)| c2[*y][*x] = true);
    print_chamber(&c2);
}

#[allow(dead_code)]
fn print_chamber(chamber: &[Vec<bool>]) {
    println!();
    let mut i = chamber.len();
    chamber.iter().rev().for_each(|x| {
        i -= 1_usize;
        let out_string = x
            .iter()
            .map(|x| if *x { '#' } else { '.' })
            .collect::<String>();
        println!("{:02} |{}|", i, out_string);
    });
    println!("   +-------+");
    println!();
}

fn find_last_filled(chamber: &[Vec<bool>]) -> usize {
    let last_filled =
        if let Some(last_filled) = chamber.iter().rev().position(|x| x.iter().any(|x| *x)) {
            chamber.len() - last_filled
        } else {
            0_usize
        };
    last_filled
}

fn adjust_chamber(chamber: &mut Vec<Vec<bool>>, rock: &Rock) {
    let lf = find_last_filled(chamber);
    let free_rows = chamber.len() - lf;
    let needed_rows = rock.max_y() + 4;

    if needed_rows > free_rows {
        for _ in 0..(needed_rows - free_rows) {
            chamber.push(vec![false; 7]);
        }
    }
}

pub fn part1(input: &[u8]) -> usize {
    let (res, _, _) = build_tower(input, 2022);
    res
}

fn find_recurrence(history: &[(usize, usize)]) -> Option<(isize, isize)> {
    let heights: Vec<_> = history.iter().map(|(_, x)| x).collect();
    let deltas: Vec<_> = heights
        .iter()
        .zip(heights.iter().skip(1))
        .map(|(x0, x1)| *x1 - *x0)
        .collect();

    for o in 0..(deltas.len() / 2_usize) {
        for r in 1..((deltas.len() - o) / 2_usize) {
            let tests = deltas
                .iter()
                .skip(o)
                .zip(deltas.iter().skip(o + r))
                .all(|(t0, tr)| tr == t0);
            if tests {
                return Some((o as isize, r as isize));
            }
        }
    }
    None
}

pub fn part2(input: &[u8]) -> usize {
    let (_, h, _) = build_tower(input, 10_000);
    let (p1, p2) = find_recurrence(&h).unwrap();
    let i = 1_000_000_000_000 % (p2);
    let n = 1_000_000_000_000 / (p2) as usize;
    let (x1, _, _) = build_tower(input, p1);
    let (x2, _, _) = build_tower(input, p2 + p1);
    let (xi, _, _) = build_tower(input, i);

    n * (x2 - x1) + xi
}

fn build_tower(input: &[u8], n_rocks: isize) -> (usize, Vec<(usize, usize)>, Vec<Vec<bool>>) {
    let mut chamber = vec![vec![false; 7]; 4];
    let rocks = vec![
        Rock::Horizontal,
        Rock::Plus,
        Rock::ReverseL,
        Rock::Vertical,
        Rock::Square,
    ];
    let mut rock_gen = rocks.iter().cycle();
    let mut jet_gen = input.iter().cycle();
    let mut fallen_rocks = 0;

    let mut history: Vec<(usize, usize)> = Vec::new();

    while fallen_rocks < n_rocks {
        let mut can_fall = true;
        let mut fall_now = false;
        let mut can_move = true;

        let mut next_move = if fall_now {
            &0_u8
        } else {
            match jet_gen.next() {
                Some(x) => x,
                x => panic!("Other infinite iterator stopped working? {:?}", x),
            }
        };

        let Some(rock) = rock_gen.next() else {
            panic!("Infinite iterator stopped working?")
        };

        adjust_chamber(&mut chamber, rock);
        let last_filled = find_last_filled(&chamber);
        history.push((fallen_rocks as usize, last_filled));

        let mut coords = (2_usize, last_filled + 3_usize);

        loop {
            if can_fall || can_move {
                if can_fall && fall_now {
                    coords = (coords.0, coords.1 - 1_usize);
                } else if can_move && !fall_now {
                    match next_move {
                        60_u8 => {
                            coords = (coords.0 - 1_usize, coords.1);
                        }
                        62_u8 => {
                            coords = (coords.0 + 1_usize, coords.1);
                        }
                        _ => panic!("Ilegal move assigned: {}", next_move),
                    }
                }
            } else {
                rock.get_abs_coordinates(&coords)
                    .iter()
                    .for_each(|(x, y)| chamber[*y][*x] = true);
                break;
            }

            fall_now = !fall_now;
            next_move = if fall_now {
                &0_u8
            } else {
                match jet_gen.next() {
                    Some(x) => x,
                    _ => panic!("Other infinite iterator stopped working?"),
                }
            };
            can_fall = rock.can_fall(&coords, &chamber);
            can_move = rock.can_move(&coords, next_move, &chamber);
        }
        fallen_rocks += 1;
    }
    (find_last_filled(&chamber), history, chamber)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 3068_usize)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 1514285714288_usize)
    }
}
