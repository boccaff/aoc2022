use hashbrown::HashSet;

#[derive(Debug, Clone)]
pub struct Blizzards {
    left: Vec<i128>,
    right: Vec<i128>,
    up: Vec<i128>,
    down: Vec<i128>,
    height: usize,
    width: u32,
    start: u32,
    end: u32,
}

type Point = (i32, i32);

fn rotate_left(x: i128, n: u32) -> i128 {
    (x >> 1) + (x & 1) * 2_i128.pow(n - 1_u32)
}

fn rotate_right(x: i128, n: u32) -> i128 {
    (x << 1) % 2_i128.pow(n) + (x << 1) / 2_i128.pow(n)
}

impl Blizzards {
    fn step(&mut self) {
        self.up.rotate_left(1);
        self.down.rotate_right(1);
        for j in 0..self.height {
            self.right[j] = rotate_right(self.right[j], self.width);
            self.left[j] = rotate_left(self.left[j], self.width);
        }
    }

    fn is_free(&self, p: &Point) -> bool {
        (self.left[p.1 as usize] >> p.0 & 1) == 0
            && (self.right[p.1 as usize] >> p.0 & 1) == 0
            && (self.down[p.1 as usize] >> p.0 & 1) == 0
            && (self.up[p.1 as usize] >> p.0 & 1) == 0
    }
}

fn generate_step((x, y): &Point, blizz: &Blizzards) -> Vec<Point> {
    [(-1, 0), (1, 0), (0, 0), (0, 1), (0, -1)]
        .into_iter()
        .map(|(dx, dy)| (x + dx, y + dy))
        .filter(|(x, y)| {
            (&0 <= x) && (*x < blizz.width as i32) && (y >= &0) && (*y < blizz.height as i32)
        })
        .collect()
}

pub fn parse_input(filename: &str) -> Blizzards {
    let mut left = Vec::new();
    let mut right = Vec::new();
    let mut up = Vec::new();
    let mut down = Vec::new();
    let mut width = 0;

    let binding = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read {:?}", filename));

    let mut lines = binding.lines();
    let height = lines.clone().count() - 2_usize;

    let first = lines.next().unwrap().to_string();
    let last = lines.clone().last().unwrap().to_string();

    let start = first.chars().position(|c| c == '.').unwrap() as u32 - 1_u32;
    let end = last.chars().position(|c| c == '.').unwrap() as u32 - 1_u32;

    for (j, line) in lines.enumerate() {
        left.push(0);
        right.push(0);
        down.push(0);
        up.push(0);

        line.chars().skip(1).enumerate().for_each(|(i, c)| {
            width = width.max(i);
            if j < height as usize {
                match c {
                    '<' => {
                        left[j] += 2_i128.pow(i as u32);
                    }
                    '>' => {
                        right[j] += 2_i128.pow(i as u32);
                    }
                    '^' => {
                        up[j] += 2_i128.pow(i as u32);
                    }
                    'v' => {
                        down[j] += 2_i128.pow(i as u32);
                    }
                    _ => {}
                }
            }
        });
    }
    left.pop();
    right.pop();
    up.pop();
    down.pop();
    let width = width as u32;

    Blizzards {
        left,
        right,
        up,
        down,
        width,
        height,
        start,
        end,
    }
}

#[allow(dead_code)]
fn print_blizz(blizz: &Blizzards, expd: &Point) {
    let width = blizz.width;
    let height = blizz.height;

    print!("#");
    for i in 0..width {
        if i == blizz.start {
            print!(".");
        } else {
            print!("#");
        }
    }
    println!("#");
    for j in 0..height {
        print!("#");
        for i in 0..width {
            if expd.0 == i as i32 && expd.1 == j as i32 {
                print!("E")
            } else {
                let left_blizz = blizz.left[j as usize] >> i & 1 == 1;
                let down_blizz = blizz.down[j as usize] >> i & 1 == 1;
                let up_blizz = blizz.up[j as usize] >> i & 1 == 1;
                let right_blizz = blizz.right[j as usize] >> i & 1 == 1;

                let winds = [left_blizz, down_blizz, up_blizz, right_blizz]
                    .iter()
                    .filter(|x| **x)
                    .count() as i32;

                if winds > 1 {
                    print!("{}", winds);
                } else if left_blizz {
                    print!("<");
                } else if down_blizz {
                    print!("v");
                } else if up_blizz {
                    print!("^");
                } else if right_blizz {
                    print!(">");
                } else {
                    print!(".")
                }
            }
        }
        println!("#")
    }
    print!("#");
    for i in 0..width {
        if i == blizz.end {
            print!(".");
        } else {
            print!("#");
        }
    }
    println!("#")
}

fn dist((x0, y0): &Point, (x1, y1): &Point) -> i32 {
    (x0 - x1).abs() + (y0 - y1).abs()
}

pub fn part1(input: &Blizzards) -> i32 {
    let mut blizz = input.clone();
    let mut candidates = HashSet::new();
    let mut i = 0_i32;

    let goal = (blizz.end as i32, blizz.height as i32);
    println!("{:?}", (blizz.end, blizz.height, blizz.start, blizz.width));
    candidates.insert((blizz.start as i32, -1_i32));

    loop {
        let mut tmp_candidates = HashSet::with_capacity(candidates.len() * 5);
        i += 1_i32;
        let curr_blizz = blizz.clone();
        blizz.step();

        for p in candidates.drain() {
            let neighbors = generate_step(&p, &curr_blizz);

            if dist(&p, &goal) == 1 {
                return i;
            } else {
                neighbors.iter().for_each(|n| {
                    if blizz.is_free(n) {
                        tmp_candidates.insert(*n);
                    }
                });
            }
        }

        candidates = tmp_candidates.clone();
    }
}

#[allow(unused_variables)]
pub fn part2(input: &Blizzards) -> i32 {
    let mut blizz = input.clone();
    let mut i = 0_i32;

    let goal = (blizz.end as i32, blizz.height as i32);
    let mut candidates = HashSet::new();
    candidates.insert((blizz.start as i32, -1_i32));

    'a: loop {
        let mut tmp_candidates = HashSet::with_capacity(candidates.len() * 5);
        i += 1_i32;
        let curr_blizz = blizz.clone();
        blizz.step();

        for p in candidates.drain() {
            let neighbors = generate_step(&p, &curr_blizz);

            if dist(&p, &goal) == 1 {
                break 'a;
            } else {
                neighbors.iter().for_each(|n| {
                    if blizz.is_free(n) {
                        tmp_candidates.insert(*n);
                    }
                });
            }
        }

        candidates = tmp_candidates.clone();
    }

    blizz.step();
    i += 1;
    let mut candidates = HashSet::new();
    let start = goal;
    candidates.insert(goal);
    let goal = (blizz.start as i32, -1_i32);

    'a: loop {
        let mut tmp_candidates = HashSet::with_capacity(candidates.len() * 5);
        i += 1_i32;
        let curr_blizz = blizz.clone();
        blizz.step();

        for p in candidates.drain() {
            let neighbors = generate_step(&p, &curr_blizz);

            if dist(&p, &goal) == 1 {
                break 'a;
            } else {
                neighbors.iter().for_each(|n| {
                    if blizz.is_free(n) {
                        tmp_candidates.insert(*n);
                    }
                });
            }
            if p == start {
                tmp_candidates.insert(p);
            }
        }
        candidates = tmp_candidates.clone();
    }

    blizz.step();
    i += 1;
    let mut candidates = HashSet::new();
    let start = goal;
    candidates.insert(goal);
    let goal = (blizz.end as i32, blizz.height as i32);

    'a: loop {
        let mut tmp_candidates = HashSet::with_capacity(candidates.len() * 5);
        i += 1_i32;
        let curr_blizz = blizz.clone();
        blizz.step();

        for p in candidates.drain() {
            let neighbors = generate_step(&p, &curr_blizz);

            if dist(&p, &goal) == 1 {
                break 'a;
            } else {
                neighbors.iter().for_each(|n| {
                    if blizz.is_free(n) {
                        tmp_candidates.insert(*n);
                    }
                });
            }
            if p == start {
                tmp_candidates.insert(p);
            }
        }
        candidates = tmp_candidates.clone();
    }

    i
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 18)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 54)
    }
}
