type Path = Vec<(usize, usize)>;
type Input = Vec<Path>;

const AIR: u8 = '.' as u8;
const SAND: u8 = 'o' as u8;
const ROCK: u8 = '#' as u8;

pub struct Grid {
    grid: Vec<Vec<u8>>,
    xmin: usize,
    height: usize,
    width: usize,
}

pub fn parse_input(filename: &str) -> Input {

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| line.split(" -> ").map(|x|
                                           {
                let (xraw, yraw) = x.split_once(",").unwrap();
                let x = xraw.parse::<usize>().unwrap();
                let y = yraw.parse::<usize>().unwrap();
                (x, y)
                                           }
                                          ).collect::<Vec<(usize, usize)>>()
             ).collect::<Vec<Vec<(usize, usize)>>>()
}

pub fn print_grid(grid: &[Vec<u8>]) {
	for line in grid.iter(){
		let strline = line.iter().map(|x| *x as char).collect::<String>();
		println!("{}", strline);
	}
}

fn build_grid(input: &[Path]) -> Grid {
    let xmin = input.iter().flatten().map(|(x, _)| x).min().unwrap();
    let xmax = input.iter().flatten().map(|(x, _)| x).max().unwrap();
    let ymax = input.iter().flatten().map(|(_, y)| y).max().unwrap();

    let width = xmax - xmin + 1;
    let height = ymax + 1;

    let mut grid = vec![vec![AIR as u8; width]; height];

    input.iter().for_each(|path| {
        path.iter().zip(path.iter().skip(1))
            .for_each(|(from, to)| {
                match (from.0 == to.0, from.1 == to.1) {
                    (true, false) => {
                        for j in from.1.min(to.1)..=from.1.max(to.1) {
                            grid[j][from.0 - xmin] = ROCK;
                        }
                    },
                    (false, true) => {
                        for i in from.0.min(to.0)..=from.0.max(to.0) {
                            grid[from.1][i - xmin] = ROCK;
                        }
                    },
                    (_, _) => panic!("Points coincide: {:?}", (from, to)),
                }
            });

    });
    Grid { grid, xmin: *xmin, height, width }
}

// check if position is available to add sand
// check if sand can move inside the cave -> move and increase sand

fn is_availabe(grid: &Grid, x: usize, y:  usize) -> bool {
    grid.grid[y][x] == AIR
}

fn wont_fall_into_abyss(grid: &Grid, pos: (usize, usize)) -> bool {
    let (x, y) = pos;
    (x > 0 && x < grid.width - 1) && (y < grid.height - 1)
}

pub fn part1(input: &[Path]) -> usize {
    let mut grid = build_grid(input);

    let mut i = 0_usize;

    'pouring: loop {

        let mut pos = (500_usize - grid.xmin, 0_usize);
        let mut moved = true;

        if is_availabe(&grid, pos.0, pos.1) {
            grid.grid[pos.1][pos.0] = SAND;
        } else {
            moved = false;
            break
        }

        while wont_fall_into_abyss(&grid, pos) && moved {

            if is_availabe(&grid, pos.0, pos.1 + 1_usize) {

                grid.grid[pos.1][pos.0] = AIR;
                pos = (pos.0, pos.1 + 1_usize);
                grid.grid[pos.1][pos.0] = SAND;

            } else if is_availabe(&grid, pos.0 - 1_usize, pos.1 + 1_usize) {

                grid.grid[pos.1][pos.0] = AIR;
                pos = (pos.0 - 1_usize, pos.1 + 1_usize);
                grid.grid[pos.1][pos.0] = SAND;

            } else if is_availabe(&grid, pos.0 + 1_usize, pos.1 + 1_usize) {

                grid.grid[pos.1][pos.0] = AIR;
                pos = (pos.0 + 1_usize, pos.1 + 1_usize);
                grid.grid[pos.1][pos.0] = SAND;

            } else {
                moved = false;
            }
        }
        if !wont_fall_into_abyss(&grid, pos) {
            grid.grid[pos.1][pos.0] = AIR;
            break 'pouring
        }

    }

    grid.grid.iter().map(|line| line.iter().filter(|x| **x == SAND).count()).sum()
}

pub fn part2(input: &[Path]) -> usize {
    let mut input = input.clone().to_vec();
    let xmin = input.iter().flatten().map(|(x, _)| x).min().unwrap();
    let xmax = input.iter().flatten().map(|(x, _)| x).max().unwrap();
    let ymax = input.iter().flatten().map(|(_, y)| y).max().unwrap();

    input.push(vec![(xmin - ymax, ymax + 2), (xmax + ymax, ymax + 2)]);

    part1(&input)
}

#[cfg(test)]

mod test {
    use super::*;

    fn test_part1(){
        let input = parse_input("input.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 24);
    }

    fn test_part2(){
        let input = parse_input("input.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 93);
    }

}
