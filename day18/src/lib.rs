use std::collections::HashSet;
type droplet = (i32, i32, i32);

fn parse_line(line: &str) -> droplet {
    let mut elems = line.split(",");
    (
        elems.next().unwrap().parse::<i32>().unwrap(),
        elems.next().unwrap().parse::<i32>().unwrap(),
        elems.next().unwrap().parse::<i32>().unwrap(),
    )
}

pub fn parse_input(filename: &str) -> HashSet<droplet> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cant find {}", filename))
        .lines()
        .map(|line| parse_line(line))
        .collect::<HashSet<_>>()
}

fn find_neighbors(t: &droplet) -> Vec<droplet> {
    vec![
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, 1),
        (-1, 0, 0),
        (0, -1, 0),
        (0, 0, -1),
    ]
    .iter()
    .map(|n| (t.0 + n.0, t.1 + n.1, t.2 + n.2))
    .collect()
}

fn count_neighbors(e: &droplet, scan: &HashSet<droplet>) -> usize {
    find_neighbors(e)
        .iter()
        .filter(|n| scan.contains(n))
        .count()
}

pub fn part1(input: &HashSet<droplet>) -> usize {
    input.iter().map(|d| 6 - count_neighbors(d, &input)).sum()
}

fn within_bounds(d: &droplet, max_coord: i32) -> bool {
    d.0 >= -1 && d.1 >= -1 && d.2 >= -1 && d.0 <= max_coord && d.1 <= max_coord && d.2 <= max_coord
}

pub fn part2(input: &HashSet<droplet>) -> usize {
    let max_coord = input.iter().map(|x| x.0.max(x.1.max(x.2))).max().unwrap() + 1;
    let mut stack = vec![(0_i32, 0_i32, 0_i32)];
    let mut spaces = HashSet::new();

    while let Some(drop) = stack.pop() {
        find_neighbors(&drop).iter().for_each(|n| {
            if !input.contains(n) && !spaces.contains(n) && within_bounds(n, max_coord) {
                stack.push(*n);
                spaces.insert(*n);
            }
        })
    }
    input.iter().map(|x| count_neighbors(x, &spaces)).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 64)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 58)
    }
}
