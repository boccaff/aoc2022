
pub fn parse_input(filename: &str) -> Vec<String> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read {}", filename))
        .lines()
        .map(|x| x.to_string())
        .collect()
}

fn eval_game(game: &str) -> isize {
    match game {
        "A X" => 3+1,
        "A Y" => 6+2,
        "A Z" => 0+3,

        "B X" => 0+1,
        "B Y" => 3+2,
        "B Z" => 6+3,

        "C X" => 6+1,
        "C Y" => 0+2,
        "C Z" => 3+3,
        ilegal => panic!("Ilegal game pattern: {}", ilegal)
    }
}

fn eval_instruction(game: &str) -> isize {
    match game {
        "A X" => 0+3,
        "A Y" => 3+1,
        "A Z" => 6+2,
        "B X" => 0+1,
        "B Y" => 3+2,
        "B Z" => 6+3,
        "C X" => 0+2,
        "C Y" => 3+3,
        "C Z" => 6+1,
        ilegal => panic!("Ilegal insturction pattern: {}", ilegal)
    }

}

pub fn part1(input: &[String]) -> isize {
    input
        .iter()
        .map(|x| eval_game(x))
        .sum()
}

pub fn part2(input: &[String]) -> isize {
    input
        .iter()
        .map(|x| eval_instruction(x))
        .sum()
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 15)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 12)
    }

}
