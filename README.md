

## Day 11

Did not find a way to solve part 2, r/adventofcode delivered the answer.

## Day 13

Learned a couple things here, using https://nickymeuleman.netlify.app/garden/aoc2022-day13 as reference.
Learning that the default behavior of list comparison match the requirements was the most surprising thing.

## Day15

Need to check a better solution for part2, or multithreaded at least.

