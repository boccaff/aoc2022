use itertools::Itertools;
use rayon::prelude::*;
use std::collections::HashMap;

use nom::{
    branch::alt,
    bytes::complete::{tag, take},
    character::complete::{alpha0, u16},
    multi::separated_list1,
    sequence::{preceded, Tuple},
    IResult,
};

fn parse_name(input: &str) -> IResult<&str, &str> {
    preceded(tag("Valve "), take(2usize))(input)
}

fn parse_flow(input: &str) -> IResult<&str, u16> {
    preceded(tag(" has flow rate="), u16)(input)
}

fn parse_connection(input: &str) -> IResult<&str, Vec<&str>> {
    let _, res = preceded(
        alt((
                tag("; tunnels lead to valves "),
                tag("; tunnel leads to valve "),
        )),
        separated_list1(tag(", "), alpha0),
    )(input);
    res
}

fn room_parser(input: &str) -> (String, u16, Vec<String>) {
    if let Ok((_, (v, f, ts))) = (parse_name, parse_flow, parse_connection).parse(input) {
        (
            v.to_string(),
            f,
            ts.iter().map(|x| x.to_string()).collect::<Vec<String>>(),
        )
    } else {
        panic!("Failed to parse:{:?}", input)
    }
}

fn parse_input(
    filename: &str,
) -> (
HashMap<String, usize>,
HashMap<usize, String>,
Vec<u16>,
Vec<Vec<u16>>,
) {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(room_parser)
        .collect::<Vec<(String, u16, Vec<String>)>>();

    let mut valve_to_pos = HashMap::with_capacity(input.len());
    let mut pos_to_valve = HashMap::with_capacity(input.len());
    let mut flows = Vec::with_capacity(input.len());
    let mut dists = vec![vec![1000; input.len()]; input.len()];

    //input.iter().enumerate().for_each(|x| {
    //println!("{:?}", x);
    //});

    input.iter().enumerate().for_each(|(i, (v, f, _))| {
        valve_to_pos.insert(v.clone(), i);
        pos_to_valve.insert(i, v.clone());
        flows.push(*f);
    });

    input.iter().for_each(|(v, _, ts)| {
        ts.iter()
            .for_each(|t| match (valve_to_pos.get(v), valve_to_pos.get(t)) {
                (Some(i), Some(j)) => {
                    dists[*i][*j] = 1_u16;
                    dists[*i][*i] = 0_u16;
                }
                _ => {
                    panic!(
                        "Failed to find match position for {:?} and {:?} on {:?}",
                        v, t, valve_to_pos
                    );
                }
            });
    });
    (valve_to_pos, pos_to_valve, flows, dists)
}

fn floyd_warshall(g: &mut Vec<Vec<u16>>) {
    let l = g.len();
    for k in 0..l {
        for i in 0..l {
            for j in 0..l {
                if g[i][j] > g[i][k] + g[k][j] {
                    g[i][j] = g[i][k] + g[k][j];
                }
            }
        }
    }
}

fn find_best_sequence(
    candidates: &[usize],
    start: usize,
    time: u16,
    release: u16,
    dists: &[Vec<u16>],
    flows: &[u16],
    visited: &[usize],
) -> (u16, Vec<usize>) {
    let mut res = candidates
        .iter()
        .filter(|curr| dists[start][**curr] < time)
        .map(|curr| {
            let time_left = time - dists[start][*curr] - 1;
            let additional_release = flows[*curr] * time_left;
            let candidates_left = candidates
                .iter()
                .filter(|candidate| *candidate != curr)
                .copied()
                .collect::<Vec<usize>>();
            let mut current_visited = visited.to_vec();
            current_visited.push(*curr);
            find_best_sequence(
                &candidates_left,
                *curr,
                time_left,
                release + additional_release,
                dists,
                flows,
                &current_visited,
            )
        })
    .collect::<Vec<(u16, Vec<usize>)>>();
    res.sort();
    if let Some(x) = res.pop() {
        x
    } else {
        (release, visited.to_vec())
    }
}

pub fn part1(filename: &str) -> u16 {
    let (vp, pv, f, mut d) = parse_input(filename);

    let valid_valves = f
        .iter()
        .enumerate()
        .filter(|(_, v)| v > &&0_u16)
        .map(|(i, _)| i)
        .collect::<Vec<usize>>();

    floyd_warshall(&mut d);
    let (r, v) = find_best_sequence(&valid_valves, vp["AA"], 30_u16, 0_u16, &d, &f, &[]);
    r
}

fn binarymask(x: i32, n_digits: u32) -> Vec<bool> {
    let mut x = x;
    let mut res = Vec::with_capacity(n_digits as usize);
    for i in 1..=n_digits {
        res.push(x / 2_i32.pow(n_digits - i) == 1);
        x = x % 2_i32.pow(n_digits - i);
    }
    res
}

pub fn part2(filename: &str) -> u16 {
    let (vp, pv, f, mut d) = parse_input(filename);

    let valid_valves = f
        .iter()
        .enumerate()
        .filter(|(_, v)| v > &&0_u16)
        .map(|(i, _)| i)
        .collect::<Vec<usize>>();

    floyd_warshall(&mut d);

    let res = (1..2_i32.pow(valid_valves.len() as u32))
        .into_par_iter()
        .map(|i| {
            let mask = binarymask(i, valid_valves.len() as u32);
            let p1 = valid_valves
                .iter()
                .zip(&mask)
                .filter(|(v, m)| **m)
                .map(|(v, _)| v)
                .copied()
                .collect::<Vec<usize>>();
            let p2 = valid_valves
                .iter()
                .zip(mask)
                .filter(|(v, m)| !m)
                .map(|(v, _)| v)
                .copied()
                .collect::<Vec<usize>>();
            let (x1, v1) = find_best_sequence(&p1, vp["AA"], 26_u16, 0_u16, &d, &f, &[]);
            let (x2, v2) = find_best_sequence(&p2, vp["AA"], 26_u16, 0_u16, &d, &f, &[]);
            x1 + x2
        })
    .collect::<Vec<u16>>();
    if let Some(x) = res.iter().max() {
        *x
    } else {
        0_u16
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let p1 = part1("example.txt");
        assert_eq!(p1, 1651_u16)
    }

    #[test]
    fn test_part2() {
        let p2 = part2("example.txt");
        assert_eq!(p2, 1707_u16)
    }

    #[test]
    fn test_parse() {
        let examples = vec![
            (
                "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
                (
                    "AA".to_string(),
                    0_u16,
                    vec!["DD".to_string(), "II".to_string(), "BB".to_string()],
                ),
            ),
            (
                "Valve HH has flow rate=22; tunnel leads to valve GG",
                ("HH".to_string(), 22_u16, vec!["GG".to_string()]),
            ),
        ];
        for (input, ans) in examples.iter() {
            let res = room_parser(input);
            assert_eq!(res, *ans)
        }
    }
}
