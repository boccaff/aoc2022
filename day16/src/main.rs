//use day16::{parse_input};
use day16::{part1, part2};

fn main() {
    let p1 = part1("input.txt");
    println!("Part 1: {}", p1);

    let p2 = part2("input.txt");
    println!("Part 2: {}", p2);
}
