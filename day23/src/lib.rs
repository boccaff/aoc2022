//use std::collections::{HashMap, HashSet};
use hashbrown::{HashMap, HashSet};

#[derive(Debug, Clone, Copy)]
enum Direction {
    N,
    S,
    E,
    W,
}

type Coords = (i32, i32);

pub fn parse_input(filename: &str) -> Vec<Coords> {
    let mut res = Vec::new();
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read {}", filename))
        .lines()
        .rev()
        .enumerate()
        .for_each(|(j, line)| {
            line.chars().enumerate().for_each(|(i, c)| {
                if c == '#' {
                    res.push((i as i32, j as i32));
                }
            });
        });
    res
}

fn get_y_range(map: &HashSet<Coords>) -> Coords {
    (
        *map.iter().map(|(_, y)| y).min().unwrap(),
        *map.iter().map(|(_, y)| y).max().unwrap(),
    )
}

fn get_x_range(map: &HashSet<Coords>) -> Coords {
    (
        *map.iter().map(|(x, _)| x).min().unwrap(),
        *map.iter().map(|(x, _)| x).max().unwrap(),
    )
}

fn print_elfs(map: &HashSet<Coords>) {
    let (mut x_min, mut x_max) = get_x_range(map);
    x_min -= 2;
    x_max += 1;
    let (mut y_min, mut y_max) = get_y_range(map);
    y_min -= 1;
    y_max += 1;

    println!("{:?}", (x_min, x_max, y_min, y_max));

    for y in (y_min..=y_max).rev() {
        for x in x_min..=x_max {
            if map.contains(&(x, y)) {
                print!("#")
            } else {
                print!(".")
            }
        }
        print!("\n")
    }
}

fn want_to_move(p: &Coords, h: &HashSet<Coords>) -> bool {
    [
        (-1, 0),
        (-1, -1),
        (0, -1),
        (1, -1),
        (1, 0),
        (1, 1),
        (0, 1),
        (-1, 1),
    ]
    .iter()
    .map(|(dx, dy)| (p.0 + dx, p.1 + dy))
    .any(|e| h.contains(&e))
}

fn free_direction(p: &Coords, d: &Direction) -> Vec<Coords> {
    match d {
        Direction::N => [(-1, 1), (0, 1), (1, 1)]
            .iter()
            .map(|(x, y)| (x + p.0, y + p.1))
            .collect(),
        Direction::S => [(-1, -1), (0, -1), (1, -1)]
            .iter()
            .map(|(x, y)| (x + p.0, y + p.1))
            .collect(),
        Direction::W => [(-1, 0), (-1, 1), (-1, -1)]
            .iter()
            .map(|(x, y)| (x + p.0, y + p.1))
            .collect(),
        Direction::E => [(1, 0), (1, 1), (1, -1)]
            .iter()
            .map(|(x, y)| (x + p.0, y + p.1))
            .collect(),
    }
}

fn simple_step(p: &Coords, d: &Direction) -> Coords {
    match d {
        Direction::N => (p.0, p.1 + 1),
        Direction::S => (p.0, p.1 - 1),
        Direction::W => (p.0 - 1, p.1),
        Direction::E => (p.0 + 1, p.1),
    }
}

fn rotate(d: &Direction) -> Direction {
    match d {
        Direction::N => Direction::S,
        Direction::S => Direction::W,
        Direction::W => Direction::E,
        Direction::E => Direction::N,
    }
}

pub fn part1(input: &[Coords]) -> i32 {
    let mut elfs = HashSet::new();

    input.iter().for_each(|elf| {
        elfs.insert(*elf);
    });

    let mut movemap: HashMap<Coords, Vec<Coords>> = HashMap::new();
    let mut global_direction = Direction::N;

    for _ in 0..10 {
        let elf_list = elfs.iter().map(|x| *x).collect::<Vec<Coords>>();
        'elfloop: for elf in elf_list {
            if want_to_move(&elf, &elfs) {
                let mut d = global_direction.clone();
                'inner: for _ in 0..4 {
                    if free_direction(&elf, &d).iter().all(|p| !elfs.contains(p)) {
                        let to = simple_step(&elf, &d);

                        movemap
                            .entry(to)
                            .and_modify(|v| v.push(elf))
                            .or_insert(vec![elf]);
                        continue 'elfloop;
                    }
                    d = rotate(&d);
                }
            }
        }

        for (to, mut from) in movemap.drain() {
            if from.len() == 1 {
                let f = from.pop().unwrap();
                elfs.insert(to);
                elfs.remove(&f);
            }
        }
        global_direction = rotate(&global_direction);
    }

    let (mut x_min, mut x_max) = get_x_range(&elfs);
    let (mut y_min, mut y_max) = get_y_range(&elfs);
    //println!("{:?}", (x_min, x_max, y_min, y_max, elfs.len()));
    //print_elfs(&elfs);
    //println!("---------------------------------");
    (x_max - x_min + 1) * (y_max - y_min + 1) - (input.len() as i32)
}

pub fn part2(input: &[Coords]) -> i32 {
    let mut elfs = HashSet::new();

    input.iter().for_each(|elf| {
        elfs.insert(*elf);
    });

    let mut movemap: HashMap<Coords, Vec<Coords>> = HashMap::new();
    let mut global_direction = Direction::N;
    let mut i = 1;

    loop {
        let elf_list = elfs.iter().map(|x| *x).collect::<Vec<Coords>>();
        'elfloop: for elf in elf_list {
            if want_to_move(&elf, &elfs) {
                let mut d = global_direction.clone();
                'inner: for _ in 0..4 {
                    if free_direction(&elf, &d).iter().all(|p| !elfs.contains(p)) {
                        let to = simple_step(&elf, &d);

                        movemap
                            .entry(to)
                            .and_modify(|v| v.push(elf))
                            .or_insert(vec![elf]);
                        continue 'elfloop;
                    }
                    d = rotate(&d);
                }
            }
        }
        if movemap.len() == 0 {
            break;
        }
        i += 1;

        for (to, mut from) in movemap.drain() {
            if from.len() == 1 {
                let f = from.pop().unwrap();
                elfs.insert(to);
                elfs.remove(&f);
            }
        }
        global_direction = rotate(&global_direction);
    }
    i
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 110)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 20)
    }
}
