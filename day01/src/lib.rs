
fn parse_block(line: &str) -> Vec<isize> {
    line
        .split("\n")
        .filter_map(|x| x.parse::<isize>().ok())
        .collect::<Vec<isize>>()
}

pub fn parse_input(filename: &str) -> Vec<Vec<isize>> {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Cannot read example.txt"))
        .split("\n\n")
        .map(|x| parse_block(x))
        .collect::<Vec<Vec<isize>>>()
}

pub fn part1(input: &[Vec<isize>]) -> isize {
    input.iter()
        .map(|x| x.iter().sum::<isize>())
        .fold(0, |acc, x| x.max(acc))
}

pub fn part2(input: &[Vec<isize>]) -> isize {
    let mut res = input.iter()
        .map(|x| x.iter().sum::<isize>())
        .collect::<Vec<isize>>();
    res.sort();
    res.iter().rev().take(3).sum()
}
