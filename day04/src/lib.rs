
use std::str::FromStr;
use std::fmt;

type ElfPair = (ElfRange, ElfRange);
type Input = Vec<ElfPair>;


#[derive(Debug, PartialEq)]
pub struct ElfRange{
    from: isize,
    to: isize,
}

impl ElfRange {
    pub fn contains(&self, other: &ElfRange) -> bool {
        self.from <= other.from && self.to >= other.to
    }

    pub fn overlap(&self, other: &ElfRange) -> bool {
        (self.from..=self.to).contains(&other.from) ||
        (self.from..=self.to).contains(&(other.to))
    }
}

fn parse_line(s: &str) -> (ElfRange, ElfRange) {
    let (e1, e2) = s.split_once(",").unwrap();
    (e1.parse().unwrap(), e2.parse().unwrap())
}


pub fn parse_input(filename: &str) -> Input {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        //.map(|x| {println!("{:?}", x);x})
        .map(|line| parse_line(line))
        .collect()
}

impl fmt::Display for ElfRange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{}", self.from, self.to - 1)
    }
}

impl FromStr for ElfRange {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (f, t) = s.split_once("-")
            .unwrap_or_else(|| panic!("Failed to split {} into a range.", s));
        Ok(
            ElfRange {
                from: f.parse::<isize>().unwrap(),
                to: t.parse::<isize>().unwrap()}
          )
}
}

pub fn part1(input: &[ElfPair]) -> usize {
    input
        .iter()
        .filter(|(e1, e2)| e1.contains(e2) || e2.contains(e1))
        .count()
}

pub fn part2(input: &[ElfPair]) -> usize {
    input
        .iter()
        .filter(|(e1, e2)| e1.overlap(e2) || e2.overlap(e1))
        .count()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_range() {
        let examples = vec![
            ("2-4", ElfRange {from: 2, to: 4}),
            ("6-8", ElfRange {from: 6, to: 8}),
            ("2-3", ElfRange {from: 2, to: 3}),
            ("4-5", ElfRange {from: 4, to: 5}),
        ];
        for (input, expected) in examples {
            assert_eq!(input.parse(), Ok(expected));
        }

    }

    #[test]
    fn test_contains() {
        let contains_examples = vec![
            ("2-8", "3-7"),
            ("4-6", "6-6"),
        ];
        for (r1, r2) in contains_examples {
            let container: ElfRange = r1.parse().unwrap();
            let containee: ElfRange = r2.parse().unwrap();

            assert!(container.contains(&containee), "Expected '{}' to contain '{}'", container, containee);
            assert!(!containee.contains(&container), "Expected '{}' to not contain '{}'", container, containee);

        }
    }

    #[test]
    fn test_overlap() {
        let contains_examples = vec![
            ("2-4", "6-8", false),
            ("2-3", "4-5", false),
            ("5-7", "7-9", true),
            ("2-8", "3-7", true),
            ("6-6", "4-6", true),
            ("2-6", "4-8", true),
        ];
        for (r1, r2, expected) in contains_examples {
            let e1: ElfRange = r1.parse().unwrap();
            let e2: ElfRange = r2.parse().unwrap();

            assert_eq!(e1.overlap(&e2), expected, "Overlap of '{}' with '{}' should be {}", e1, e2, expected);

        }
    }

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let e1 = part1(&input);
        assert_eq!(e1, 2)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let e2 = part2(&input);
        assert_eq!(e2, 4)
    }
}
