use std::collections::HashSet;

type Input = Vec<(char, isize)>;
type Knot = (isize, isize);
type Rope = Vec<Knot>;

pub fn parse_input(filename: &str) -> Input {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|x| {
            let (d, n) = x.split_once(" ").unwrap();
            let mut d = d.chars();
            let n = n.parse::<isize>().unwrap();
            (d.next().unwrap(), n)
        })
        .collect()
}

fn move_head(knot: &Knot, motion: char) -> Knot {
    let mut knot = *knot;
    match motion {
        'R' => knot.0 += 1,
        'L' => knot.0 -= 1,
        'U' => knot.1 += 1,
        'D' => knot.1 -= 1,
        _ => panic!("Invalid motion: '{}'", motion),
    }
    knot
}

fn update_tail(head: &Knot, tail: &Knot) -> Knot {
    let mut dx = head.0 - tail.0;
    let mut dy = head.1 - tail.1;

    if dx.abs() >= 2 || dy.abs() >= 2 {
        if dx != 0 {
            dx = dx.signum()
        }
        if dy != 0 {
            dy = dy.signum()
        }
        (tail.0 + dx, tail.1 + dy)
    } else {
        *tail
    }
}


pub fn part1(input: &Input) -> usize {
    let mut rope = vec![(0, 0); 2];
    let mut set: HashSet<Knot> = HashSet::new();

    for (motion, n) in input.iter() {
        for i in 0..*n {
            rope[0] = move_head(&rope[0], *motion);
            rope[1] = update_tail(&rope[0], &rope[1]);
            set.insert(rope[1]);
        }

    }

    set.len()

}


pub fn part2(input: &Input) -> usize {
    let mut rope = vec![(0, 0); 10];
    let mut pos_tracker: Vec<Knot> = Vec::new();

    for (motion, n) in input.iter() {
        for i in 0..*n {
            rope[0] = move_head(&rope[0], *motion);

            for i in 1_usize..rope.len() {
                rope[i] = update_tail(&rope[i - 1_usize], &rope[i]);
            }

            pos_tracker.push(*rope.last().unwrap());
        }

    }

    let set: HashSet<(isize, isize)> = pos_tracker.into_iter().collect();
    set.len()

}


fn print_positions(head: &(isize, isize), tail: &(isize, isize), visited: &[(isize, isize)]) {
    let xmax = head.0
        .max(tail.0)
        .max(visited.iter().map(|(x, _)| *x).max().unwrap_or(isize::MIN))
        .max(6);
    let ymax = head.1
        .max(tail.1)
        .max(visited.iter().map(|(y, _)| *y).max().unwrap_or(isize::MIN))
        .max(6);

    for j in (0..ymax).rev() {
        for i in (0..xmax) {
            if i == head.0 && j == head.1 {
                print!("H");
            } else if i == tail.0 && j == tail.1 {
                print!("T");
            } else if visited.contains(&(i, j)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        print!("\n")
    }

}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 13)
    }

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 1);

        let input = parse_input("example2.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 36)
    }
}
