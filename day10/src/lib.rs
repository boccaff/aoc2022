pub type Input = Vec<Instruction>;

#[derive(Debug)]
pub enum Instruction {
    Noop,
    Add(isize),
}

pub fn parse_input(filename: &str) -> Input {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line|{
            match &line[..4] {
                "noop" => Instruction::Noop,
                "addx" => Instruction::Add(line[5..].parse::<isize>().unwrap()),
                _ => panic!("Unknown instruction: {}", line)
            }
        }
        ).collect()
}

fn build_signal(input: &Input) -> Vec<isize>{
    let mut signal = vec![1];
    //let mut signal = Vec::new();

    input.iter()
        .for_each(|instr| {
            match instr {
                Instruction::Noop => {
                    signal.push(0);
                },
                Instruction::Add(x) => {
                    signal.push(0);
                    signal.push(*x);
                },
            }
        });
    signal
}

pub fn part1(input: &Input) -> isize {

    let signal = build_signal(input);
    let mut ans = 0;
    let mut running_total = 0;

    for (i, x) in signal.iter().enumerate() {
        if i == 20 || (i > 20 && (i as isize - 20) % 40 == 0){
            ans += i as isize * running_total;
        }
        running_total += x;
    }

    ans
}

pub fn part2(input: &Input) {

    let signal = build_signal(input);
    let mut running_total = 0;
    let mut sprite = 0;

    for i in 0..signal.len() {

        if sprite % 40 == 0 {
            sprite = 0;
            print!("\n");
        }

        running_total += signal[i];

        if ((running_total - 1)..(running_total+2)).contains(&sprite) {
            print!("#");
        } else {
            print!(" ");
        }
        sprite += 1;

    }

}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 13140)
    }
}
