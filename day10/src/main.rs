use day10::{parse_input, part1, part2};

fn main() {

    let input = parse_input("input.txt");
    let p1 = part1(&input);
    println!("Part 1: {}", p1);

    let ex = parse_input("input.txt");
    part2(&ex);

}
