
pub fn parse_input(filename: &str) -> Vec<Vec<u8>>{
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| line.chars().map(|x| x as u32 - '0' as u32).map(|x| x as u8).collect::<Vec<u8>>())
        .collect()
}

fn is_visible(height: u8, x: usize, y: usize, map: &[Vec<u8>]) -> bool {
    let m = map.len();
    let n = map.first().unwrap().len();
    ((x + 1)..m).map(|x| x as usize).all(|xi| map[xi][y] < height) ||
    (0..x).all(|xi| map[xi][y] < height) ||
    ((y + 1)..n).all(|yi| map[x][yi] < height) ||
    (0..y).all(|yi| map[x][yi] < height)
}

pub fn part1(input: &[Vec<u8>]) -> usize {
    input.iter()
        .enumerate()
        .map(|(i, line)|
             line.iter()
             .enumerate()
             .filter(|(j, x)| is_visible(**x, i, *j, input))
             .count()
             )
        .sum()
}


fn scenic_score(height: u8, x: usize, y: usize, map: &[Vec<u8>]) -> usize {
    let m = map.len();
    let n = map.first().unwrap().len();

    let mut r = 0;
    let mut l = 0;
    let mut d = 0;
    let mut u = 0;

    for yi in (0..y).rev() {
        l += 1;
        if map[x][yi] >= height {
            break
        }
    }

    for xi in (x + 1)..m {
        d += 1;
        if map[xi][y] >= height {
            break
        }
    }

    for xi in (0..x).rev() {
        r += 1;
        if map[xi][y] >= height {
            break
        }
    }


    for yi in (y + 1)..n {
        u += 1;
        if map[x][yi] >= height {
            break
        }
    }

    r * l * d * u
}

pub fn part2(input: &[Vec<u8>]) -> usize {
    input.iter()
        .enumerate()
        .map(|(i, line)|
             line.iter()
             .enumerate()
             .map(|(j, x)| scenic_score(*x, i, j, input))
             .max().unwrap()
             )
        .max().unwrap()
}

#[cfg(test)]

mod test {
    use super::*;

    #[test]
    fn test_part1(){
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 21)
    }

    #[test]
    fn test_scenic_score(){
        let input = parse_input("example.txt");
        let examples = vec![
            (5, 1, 2, 4),
            (5, 3, 2, 8),
        ];
        for (h, i, j, expected) in examples.iter(){
            let ans = scenic_score(*h, *i, *j, &input);
            assert_eq!(ans, *expected, "For {}, ({},{}) found: {}, expected {}", *h, *i, *j, ans, *expected)
        }
    }

    #[test]
    fn test_part2(){
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 8)
    }

}
