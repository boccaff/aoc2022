use std::collections::{HashMap, HashSet};

use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::{branch::alt, combinator::map, multi::many_till, IResult};

#[derive(Debug, Eq, PartialEq)]
pub enum Move {
    Number(isize),
    TurnLeft,
    TurnRight,
}

#[derive(Debug, Eq, PartialEq, Clone, Hash, Copy)]
enum Direction {
    U,
    D,
    L,
    R,
}

type Grid = HashMap<(i32, i32), bool>;
type Route = Vec<Move>;
type Inputs = (Grid, Route);
type Warp = HashMap<(i32, i32, Direction), (i32, i32, Direction)>;

fn path_parser(input: &str) -> IResult<&str, (Route, &str)> {
    many_till(
        alt((
            map(digit1, |s: &str| Move::Number(s.parse::<isize>().unwrap())),
            map(tag("R"), |_| Move::TurnRight),
            map(tag("L"), |_| Move::TurnLeft),
        )),
        tag("\n"),
    )(input)
}

fn grid_parser(input: &str) -> Grid {
    let mut grid = HashMap::new();
    input.lines().enumerate().for_each(|(y, line)| {
        line.chars().enumerate().for_each(|(x, c)| match c {
            ' ' => {}
            '.' => {
                grid.insert((x as i32, y as i32), true);
            }
            '#' => {
                grid.insert((x as i32, y as i32), false);
            }
            x => panic!("Bad gliph on board: {}", x),
        })
    });
    grid
}

pub fn parse_input(filename: &str) -> Inputs {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename));

    let (grid, route) = input.split_once("\n\n").unwrap();

    let Ok((_, (route, _))) = path_parser(route) else {
        panic!("Failed to parse path.")
    };

    let grid = grid_parser(grid);
    (grid, route)
}

fn find_grid_limits(grid: &Grid) -> (i32, i32) {
    let max_y = grid.keys().map(|(_, y)| *y).max().unwrap();
    let max_x = grid.keys().map(|(x, _)| *x).max().unwrap();
    (max_x, max_y)
}

fn print_space(b: bool) {
    if b {
        print!(".")
    } else {
        print!("#")
    }
}

fn print_grid(grid: &Grid, path: &HashMap<(i32, i32), char>) {
    let (max_x, max_y) = find_grid_limits(grid);
    (0..=max_y).for_each(|y| {
        (0..=max_x).for_each(|x| {
            if path.contains_key(&(x, y)) {
                print!("{}", path[&(x, y)]);
            } else if grid.contains_key(&(x, y)) {
                print_space(grid[&(x, y)])
            } else {
                print!(" ")
            }
        });
        print!("\n");
    })
}

fn find_start(grid: &Grid) -> (i32, i32) {
    let (max_x, max_y) = find_grid_limits(grid);
    for y in 0..=max_y {
        for x in 0..max_x {
            if grid.contains_key(&(x, 0)) {
                return (x, y);
            }
        }
    }
    unreachable!()
}

fn simple_step(pos: &(i32, i32), direction: &Direction) -> (i32, i32) {
    match direction {
        Direction::U => (pos.0, pos.1 - 1),
        Direction::D => (pos.0, pos.1 + 1),
        Direction::L => (pos.0 - 1, pos.1),
        Direction::R => (pos.0 + 1, pos.1),
    }
}

fn change_direction(direction: &Direction, mv: &Move) -> Direction {
    match mv {
        Move::TurnLeft => match direction {
            &Direction::U => {
                return Direction::L;
            }
            &Direction::D => {
                return Direction::R;
            }
            &Direction::L => {
                return Direction::D;
            }
            &Direction::R => {
                return Direction::U;
            }
        },
        Move::TurnRight => match direction {
            &Direction::U => {
                return Direction::R;
            }
            &Direction::D => {
                return Direction::L;
            }
            &Direction::L => {
                return Direction::U;
            }
            &Direction::R => {
                return Direction::D;
            }
        },
        _ => panic!("Linear movement {:?} passed to change direction", mv),
    }
}

fn calc_answer(coord: &(i32, i32), direction: &Direction) -> i32 {
    let mut res = (coord.1 + 1) * 1000 + (coord.0 + 1) * 4;
    match direction {
        Direction::U => {
            res += 3;
        }
        Direction::D => {
            res += 1;
        }
        Direction::L => {
            res += 2;
        }
        Direction::R => {
            res += 0;
        }
    }
    res
}

fn generate_neighbor8(pos: &(i32, i32)) -> Vec<(i32, i32)> {
    [
        (-1, 0),
        (-1, -1),
        (0, -1),
        (1, -1),
        (1, 0),
        (1, 1),
        (0, 1),
        (-1, 1),
    ]
    .iter()
    .map(|(dx, dy)| (pos.0 + dx, pos.1 + dy))
    .collect()
}

fn generate_neighbor4(pos: &(i32, i32)) -> Vec<(i32, i32)> {
    [(-1, 0), (0, -1), (1, 0), (0, 1)]
        .iter()
        .map(|(dx, dy)| (pos.0 + dx, pos.1 + dy))
        .collect()
}

fn find_perimeter(grid: &Grid) -> HashSet<(i32, i32)> {
    let mut res = HashSet::new();
    grid.keys().for_each(|k| {
        generate_neighbor8(k).iter().for_each(|n| {
            if !grid.contains_key(n) {
                res.insert(*k);
            }
        })
    });
    res
}

fn calc_wrap1(grid: &Grid) -> Warp {
    let per = find_perimeter(grid);
    let (max_x, max_y) = find_grid_limits(grid);
    let mut res = HashMap::new();
    'a: for p in per.iter() {
        for d in [Direction::U, Direction::D, Direction::L, Direction::R].iter() {
            let step = simple_step(p, d);
            if !grid.contains_key(&step) {
                match d {
                    Direction::U => {
                        for y in (0..=max_y).rev() {
                            if grid.contains_key(&(p.0, y)) {
                                res.insert((p.0, p.1, d.clone()), (p.0, y, *d));
                                continue 'a;
                            }
                        }
                    }
                    Direction::D => {
                        for y in 0..=max_y {
                            if grid.contains_key(&(p.0, y)) {
                                res.insert((p.0, p.1, d.clone()), (p.0, y, *d));
                                continue 'a;
                            }
                        }
                    }
                    Direction::L => {
                        for x in (0..=max_x).rev() {
                            if grid.contains_key(&(x, p.1)) {
                                res.insert((p.0, p.1, d.clone()), (x, p.1, *d));
                                continue 'a;
                            }
                        }
                    }
                    Direction::R => {
                        for x in 0..=max_x {
                            if grid.contains_key(&(x, p.1)) {
                                res.insert((p.0, p.1, d.clone()), (x, p.1, *d));
                                continue 'a;
                            }
                        }
                    }
                }
            }
        }
    }
    res
}

fn find_inner_nodes(per: &HashSet<(i32, i32)>, grid: &Grid) -> Vec<(i32, i32)> {
    let mut res = Vec::new();
    per.iter().for_each(|p| {
        if generate_neighbor8(p)
            .iter()
            .filter(|n| grid.contains_key(n))
            .count()
            == 7
        {
            res.push(*p)
        }
    });
    res
}

fn is_corner(p: &(i32, i32), grid: &Grid) -> bool {
    generate_neighbor4(p)
        .iter()
        .filter(|x| grid.contains_key(x))
        .count()
        == 2
}

fn corner_find_out(p: &(i32, i32), grid: &Grid, m: &Move) -> Direction {
    let mut res = Vec::new();
    for d in [Direction::U, Direction::D, Direction::R, Direction::L] {
        if !grid.contains_key(&simple_step(p, &d)) {
            res.push(d);
        }
    }
    let mut iter = res.iter();
    let v1 = *iter.next().unwrap();
    let v2 = *iter.next().unwrap();
    if change_direction(&v1, &m) == v2 {
        return v2;
    } else {
        return v1;
    }
}

fn simple_find_out(p: &(i32, i32), grid: &Grid) -> Direction {
    for d in [Direction::U, Direction::D, Direction::R, Direction::L] {
        if !grid.contains_key(&simple_step(p, &d)) {
            return d;
        }
    }
    unreachable!()
}

fn perimeter_step(p: &(i32, i32), grid: &Grid, turn: Move) -> (i32, i32) {
    let d_out;
    if is_corner(p, grid) {
        d_out = corner_find_out(p, grid, &turn);
    } else {
        d_out = simple_find_out(p, grid);
    }
    simple_step(p, &change_direction(&d_out, &turn))
}

fn calc_reverse_direction(d: &Direction) -> Direction {
    match *d {
        Direction::U => Direction::D,
        Direction::D => Direction::U,
        Direction::R => Direction::L,
        Direction::L => Direction::R,
    }
}

fn char_from_direction(d: &Direction) -> char {
    match *d {
        Direction::U => '^',
        Direction::D => 'v',
        Direction::R => '>',
        Direction::L => '<',
    }
}

fn find_first_direction(p: &(i32, i32), per: &HashSet<(i32, i32)>) -> ((i32, i32), Direction) {
    for d in [Direction::U, Direction::D, Direction::R, Direction::L] {
        let q = simple_step(p, &d);
        if per.contains(&q) {
            return (q, d);
        }
    }
    unreachable!("{:?} must be a Inner Point", p)
}

fn find_first_points(p: &(i32, i32), per: &HashSet<(i32, i32)>) -> ((i32, i32), (i32, i32)) {
    let v2: (i32, i32);
    let d2: Direction;

    let (v1, d1) = find_first_direction(p, per);

    if per.contains(&simple_step(p, &change_direction(&d1, &Move::TurnLeft))) {
        d2 = change_direction(&d1, &Move::TurnLeft);
        v2 = simple_step(p, &d2);
    } else {
        d2 = change_direction(&d1, &Move::TurnRight);
        v2 = simple_step(p, &d2);
    }

    if d1 == change_direction(&d2, &Move::TurnLeft) {
        return (v1, v2);
    } else {
        return (v2, v1);
    }
}

fn calc_wrap2(grid: &Grid) -> HashMap<(i32, i32, Direction), (i32, i32, Direction)> {
    let mut res = HashMap::new();

    let per = find_perimeter(grid);
    let inn = find_inner_nodes(&per, grid);

    for p in inn.iter() {
        let (mut p1, mut p2) = find_first_points(p, &per);

        //let mut print_dict = HashMap::new();
        //print_dict.insert(*p, 'X');
        //print_dict.insert(p1, '1');
        //print_dict.insert(p2, '2');
        //print_grid(grid, &print_dict);

        //let mut p1_1 = *p;
        //let mut p1_2 = *p;
        let mut out_p1 = simple_find_out(&p1, grid);

        //let mut p2_1 = *p;
        //let mut p2_2 = *p;
        let mut out_p2 = simple_find_out(&p2, grid);

        let mut skipped_p1 = false;
        let mut skipped_p2 = false;

        let mut i = 0;

        while ((!is_corner(&p1, grid) || !is_corner(&p2, grid)) || (!skipped_p1 || !skipped_p2)) {
            //print_dict.insert(p1, '1');
            //print_dict.insert(p2, '2');
            //print_dict.insert(p1, char::from_digit(i % 10, 10_u32).unwrap());
            //print_dict.insert(p2, char::from_digit(i % 10, 10_u32).unwrap());

            res.insert(
                (p1.0, p1.1, out_p1),
                (p2.0, p2.1, calc_reverse_direction(&out_p2)),
            );
            res.insert(
                (p2.0, p2.1, out_p2),
                (p1.0, p1.1, calc_reverse_direction(&out_p1)),
            );
            //println!("{:?}", (i, (p1.0, p1.1, out_p1), (p2.0, p2.1, out_p2)));
            //i += 1;

            if !is_corner(&p1, grid) || skipped_p1 {
                //p1_2 = p1_1;
                //p1_1 = p1;
                p1 = perimeter_step(&p1, grid, Move::TurnLeft);
                out_p1 = simple_find_out(&p1, grid);
                skipped_p1 = false;
            } else {
                out_p1 = change_direction(&out_p1, &Move::TurnLeft);
                skipped_p1 = true;
            }

            if !is_corner(&p2, grid) || skipped_p2 {
                //p2_2 = p2_1;
                //p2_1 = p2;
                p2 = perimeter_step(&p2, grid, Move::TurnRight);
                out_p2 = simple_find_out(&p2, grid);
                skipped_p2 = false;
            } else {
                out_p2 = change_direction(&out_p2, &Move::TurnRight);
                skipped_p2 = true;
            }

            //if i == 16 {
            //println!("!");
            //break;
            //}

            //println!("{:?}", (p1, p2, i));
        }
        //print_grid(grid, &print_dict);
        //println!("----------------------------------------------------------------------");
    }
    res
}

fn check_move(
    grid: &Grid,
    pos: &(i32, i32),
    direction: &Direction,
    warp: &Warp,
) -> (bool, (i32, i32), Direction) {
    let candidate = simple_step(pos, direction);
    if grid.contains_key(&candidate) {
        if grid[&candidate] {
            return (true, candidate, *direction);
        } else {
            return (false, *pos, *direction);
        }
    } else {
        //println!("##############################################");
        //println!("Last key: {:?}", (pos.0, pos.1, *direction));
        //print_grid(grid, &HashMap::from([((pos.0, pos.1), 'K')]));

        let wrapped = warp[&(pos.0, pos.1, *direction)];

        if grid[&(wrapped.0, wrapped.1)] {
            return (true, (wrapped.0, wrapped.1), wrapped.2);
        } else {
            return (false, *pos, *direction);
        }
    }
}

pub fn part1(input: &Inputs) -> i32 {
    let (grid, route) = input;
    let warp = calc_wrap1(grid);
    let mut coord = find_start(grid);
    let mut direction = Direction::R;
    for mv in route {
        match mv {
            Move::Number(x) => {
                for _ in 0..*x {
                    let (res, pos, _) = check_move(grid, &coord, &direction, &warp);
                    if res {
                        coord = pos;
                        continue;
                    } else {
                        break;
                    }
                }
            }
            _ => direction = change_direction(&direction, mv),
        }
    }
    calc_answer(&coord, &direction)
}

pub fn part2(input: &Inputs) -> i32 {
    let (grid, route) = input;
    let warp = calc_wrap2(grid);
    //for (k, v) in warp.iter() {
    //println!("{:?}", (k, v));
    //}

    let mut coord = find_start(grid);
    let mut direction = Direction::R;
    //let mut print_dict = HashMap::new();

    //print_dict.insert(coord, char_from_direction(&direction));
    for mv in route {
        match mv {
            Move::Number(x) => {
                for _ in 0..*x {
                    let (res, pos, dir) = check_move(grid, &coord, &direction, &warp);
                    //print_dict.insert(coord, char_from_direction(&direction));
                    if res {
                        coord = pos;
                        direction = dir;
                        continue;
                    } else {
                        //print_dict.insert(coord, char_from_direction(&direction));
                        break;
                    }
                }
            }
            _ => {
                direction = change_direction(&direction, mv);
                //print_dict.insert(coord, char_from_direction(&direction));
            }
        }

        //println!("----------------------------------------------------------------------");
        //println!("{:?}", (coord, direction, mv));
        //print_grid(grid, &print_dict);
    }
    //println!("----------------------------------------------------------------------");
    //println!("{:?}", (coord, direction, mv));
    //print_grid(grid, &print_dict);
    calc_answer(&coord, &direction)
}

#[cfg(test)]
mod test {
    use super::*;

    //#[test]
    //fn test_turned() {
    //let p = (1, 0);
    //let q = (2, 0);
    //let r = (2, 1);
    //let grid = HashMap::from([
    //((0, 0), true),
    //((1, 0), true),
    //((2, 0), true),
    //((2, 1), true),
    //((2, 2), true),
    //((1, 1), true),
    //]);
    //assert!(turned(&p, &q, &r, &grid))
    //}

    #[test]
    fn test_part1() {
        let input = parse_input("example.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 6032)
    }

    #[test]
    fn test_part2() {
        let input = parse_input("example.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 5031)
    }
}
