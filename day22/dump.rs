fn simple_step(pos: &(i32, i32), direction: &Direction) -> (i32, i32) {
    match direction {
        Direction::U => (pos.0, pos.1 - 1),
        Direction::D => (pos.0, pos.1 + 1),
        Direction::L => (pos.0 - 1, pos.1),
        Direction::R => (pos.0 + 1, pos.1),
    }
}

fn wrap_step(pos: &(i32, i32), direction: &Direction, grid: &Grid) -> (i32, i32) {
    let (max_x, max_y) = find_grid_limits(grid);
    let mut on_x;
    let mut ascending;
    match direction {
        Direction::U => {
            on_x = false;
            ascending = false;
        }
        Direction::D => {
            on_x = false;
            ascending = true;
        }
        Direction::L => {
            on_x = true;
            ascending = false;
        }
        Direction::R => {
            on_x = true;
            ascending = true;
        }
    }

    let range = 0..=(if on_x { max_x } else { max_y });

    if ascending {
        for w in range {
            let mut coord;
            if on_x {
                coord = (w, pos.1)
            } else {
                coord = (pos.0, w)
            };
            if grid.contains_key(&coord) && grid[&coord] {
                return coord;
            }
        }
    } else {
        for w in range.rev() {
            let mut coord;
            if on_x {
                coord = (w, pos.1)
            } else {
                coord = (pos.0, w)
            };
            if grid.contains_key(&coord) && grid[&coord] {
                return coord;
            }
        }
    }

    unreachable!()
}

// fn step
// if stepping move outside the map, check if you can wrap

fn check_move(grid: &Grid, pos: &(i32, i32), direction: &Direction) -> (bool, (i32, i32)) {
    let candidate = simple_step(pos, direction);
    if grid.contains_key(&candidate) {
        return (true, candidate);
    } else {
        let wrapped = wrap_step(pos, direction, grid);
        if grid[&wrapped] {
            return (true, candidade);
        }
    }
    true
}
